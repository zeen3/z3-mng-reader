/// <reference lib="../../node_modules/types-serviceworker/index.d.ts">
function concat(init: ResponseInit, ...streams: Promise<Body>[]) {
	const {readable, writable} = new TransformStream<Uint8Array, Uint8Array>();
	streams.reduce(
		(a, b) => Promise.all([b, a]).then(
			body => body[0].body
			&& body[0].body.pipeTo(writable, {preventClose: true}).then(_ => null)
			|| null
		),
		Promise.resolve<null>(null)
	).then(() => writable.getWriter().close());
	return new Response(readable, init)
}
interface Signal {
	signal: AbortSignal;
}
let truthy: ((v: any) => boolean) = v => !!v;
/** A _real_ promise race function (requires OK) */
const realPromiseRace = <T = any, TO = Exclude<T, undefined | false | 0 | null | ''>>(
	proms: Iterable<T | PromiseLike<T>>,
	ok: (v: T) => boolean | undefined = truthy,
	signal: Signal | null = null
): Promise<TO> => new Promise((r, j) => {
	let rj = 0
	let onsignal: null | (() => void) = null;
	if (signal) signal.signal.addEventListener("abort", onsignal = () => {
		j(new DOMException("The user aborted a request.", "AbortError"))
		errs.length = 0
	}, {once: true});
	const onerr = <E>(e: E): E => (rj--, e);
	const onok = (t: T): void | T =>
	// identity if we've already returned
	rj-- > 0 && ok(t)
	? (
		// set length to 0 for fast gc where possible
		errs.length = rj = 0,
		// remove abort signal
		onsignal && signal!.signal.removeEventListener('abort', onsignal),
		onsignal = null, signal = null,
		// return type
		r(t as unknown as TO)
	) : t;
	const errs: Promise<any>[] = Array.from<T | PromiseLike<T>, Promise<void | T>>(
		proms,
		v => Promise.resolve(v).then(onok, onerr)
	);
	rj = errs.length
	Promise.all(errs).then(j)
})
/** Adds a content type to a known output value. */
const ct = (type: string, ...b: BlobPart[]) => new Blob(b, {type})
function fetchHandler(r: Request, url: URL): void | Promise<Response> {
	const pn = url.pathname.slice(1).split('/').reverse()
	switch (pn.pop()) {
		case 'local-settings':
			return caches.open("local-settings")
				.then(c => c.match(r))
				.then(r => r || new Response(
					ct('application/javascript'),
					{ status: 204 }
				));
		case 'api':
			return realPromiseRace(
				[
					caches.match(r, {cacheName: 'api-cache'}),
					fetch(r, { cache: 'no-cache' })
				],
				v => v && v.status >= 200 && v.status < 300
			).catch(_ => _[1].status >= 400 && _[1].status < 500 ? _[1] : new Response(
				ct('application/json', JSON.stringify(_)),
				{ status: 418 }
			))
	}
}

addEventListener("fetch", (e: any) => {
	const r = e.request as Request
	const url = new URL(r.url)
	const resp = fetchHandler(r, url);
	resp && e.respondWith(resp);
})
