
import { readerZ3 } from "../common/db-init";
import { Actions } from "../common/enums-interf";
import { LooseKeyCodeActions, BroadcastChannelKeyed } from "../common/keycodes";

let keyCodeActions: LooseKeyCodeActions;
async function reloadKeyCodeActions(): Promise<void> {
	keyCodeActions || (keyCodeActions = {});
	let cursor = await (await readerZ3())
	.transaction("key-binds", "readonly")
	.objectStore("key-binds")
	.openCursor();

	while (cursor) {
		keyCodeActions[cursor.key] = cursor.value;
		cursor = await cursor.continue();
	}
}
new BroadcastChannelKeyed('key-code-actions').onmessage = async e => {
	const {code, mod, action} = e.data;
	keyCodeActions || await reloadKeyCodeActions();
	(keyCodeActions[code] || (
		keyCodeActions[code] = new Uint8Array(16) as Actions
		))[mod] = action;
	const blob = new Blob( [
		'var KEY_BINDINGS = {',
		Object.keys(keyCodeActions).map(n => `${
			JSON.stringify(n)}:Uint8Array.of(${
				keyCodeActions[n].join(',')})`).join(','),
		'};',
	], { type: 'application/javascript' });
	await (await caches.open('local-settings')).put(
		'/local-settings/key-code-actions.js',
		new Response(blob, {headers: {'content-length': String(blob.size)}})
	);
}
new BroadcastChannelKeyed('scroll-speeds').onmessage = async e => {
	const blob = new Blob(
		[`var SCROLL_SPEED = {x:${Math.abs(e.data[0])},y:${Math.abs(e.data[1])}};` ],
		{type: 'application/json'}
	);
	await (await caches.open('local-settings')).put(
		'/local-settings/scroll-speeds.js',
		new Response(blob, {headers: {'content-length': String(blob.size)}})
	);
}
