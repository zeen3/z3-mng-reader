import { readerZ3 } from "../common/db-init";
import { scr, dec, can as cancel } from "./scroller";
import { LooseKeyCodeActions, KEY_BINDINGS, MessageEventEnum, BroadcastChannelKeyed } from '../common/keycodes';
import { Mod, Action, Actions } from '../common/enums-interf';

export function performAction<T extends Event>(action: Action, e?: T) {
	switch (action) {
		case Action.None: break;

		case Action.ScrollDown:
		case Action.ScrollRight:
		case Action.ScrollUp:
		case Action.ScrollLeft:
			if (e && ((e instanceof KeyboardEvent && !e.repeat) ||
				!(e instanceof KeyboardEvent))) scr(e, action - Action.ScrollDown);
			else scr({timeStamp: performance.now()}, action - Action.ScrollDown);
			break;
	}
}
export function performedAction(action: Action) {
	switch (action) {
		case Action.None: break;

		case Action.ScrollDown:
		case Action.ScrollRight:
		case Action.ScrollUp:
		case Action.ScrollLeft:
			dec(action - Action.ScrollDown);
			break;
	}
}
export let keyCodeActions = new BroadcastChannelKeyed('key-code-actions');
function keyCodeActionsSet(e: MessageEventEnum<"key-code-actions">) {
	const {code, mod, action} = e.data;
	(keyBinds[code] || (
		keyBinds[code] = new Uint8Array(16) as Actions))[mod] = action;
}
function resetBinds() {
	let kb = {} as LooseKeyCodeActions;
	keyCodeActions = new BroadcastChannelKeyed('key-code-actions');
	readerZ3().then(db => db
		.transaction("key-binds", "readonly")
		.objectStore("key-binds")
		.openCursor()).then(async cur => {
			while (cur) {
				kb[cur.key] = cur.value;
				cur = await cur.continue();
			}
			keyBinds = kb;
			keyCodeActions.onmessage = keyCodeActionsSet;
		});
	return kb;
}
let keyBinds: LooseKeyCodeActions = typeof KEY_BINDINGS === 'object' ?
	KEY_BINDINGS as LooseKeyCodeActions :
	resetBinds();

{
	let freeze = () => keyCodeActions.close();
	addEventListener('freeze', freeze, true);
	addEventListener('pagehide', freeze, true);
	addEventListener('resume', resetBinds, true);
	addEventListener('pageshow', resetBinds, true);
}

document.addEventListener('keydown', e => {
	if ('value' in e.target!) cancel();
	else if (e.code in keyBinds) {
		const mod: Mod = (
			(e.shiftKey ? Mod.Shift : Mod.None) |
			(e.metaKey  ? Mod.Meta  : Mod.None) |
			(e.ctrlKey  ? Mod.Ctrl  : Mod.None) |
			(e.altKey   ? Mod.Alt   : Mod.None)
		);
		const action = keyBinds[e.code][mod];
		performAction(action, e);
	}
})
