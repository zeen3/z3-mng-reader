import { SCROLL_SPEED, BroadcastChannelKeyed } from '../common/keycodes';

const dirs = new Uint8ClampedArray(4)
const _d = new Uint32Array(dirs.buffer, dirs.byteOffset, 1)
const dtxy = typeof SCROLL_SPEED === 'object' ? SCROLL_SPEED : {x: 0.84, y: 0.94};
let t0 = performance.now()
let raf = 0
/** direction, default is anticlockwise starting from bottom (down). */
export const enum Dirs { Down, Right, Up, Left }
function scroller(T: DOMHighResTimeStamp) {
	if (_d[0] !== 0) {
		const dt = Math.min(T - t0, 50);
		const dx = (dirs[Dirs.Down] - dirs[Dirs.Up]);// & 0x800000ff;
		const dy = (dirs[Dirs.Right] - dirs[Dirs.Left]);// & 0x800000ff;
		const sc = document.fullscreenElement || document.scrollingElement || document.body;
		sc.scrollBy(dt * dtxy.x * dx, dt * dtxy.y * dy);
		t0 = T;
		raf = requestAnimationFrame(scroller);
	} else raf = 0;
}

interface TimeStamp { timeStamp: DOMHighResTimeStamp; }
/** scroll loop */
export function scr<T extends TimeStamp>(e: T, d: Dirs) {
	++dirs[d];
	raf || (
		raf = requestAnimationFrame(scroller),
		t0 = e.timeStamp
	);
}
/** decrements the direction */
export function dec(d: Dirs) { --dirs[d]; }
/** clears the animation for later use */
export function can() {
	cancelAnimationFrame(raf)
	_d[0] = raf = 0
}
/** set x and y speeds */
export function sxy(x: number, y: number) {
	dtxy.x = Math.abs(x);
	dtxy.y = Math.abs(y);
}
document.addEventListener('visibilitychange', can)

const bc = new BroadcastChannelKeyed('scroll-speeds')
bc.onmessage = e => sxy(e.data[0], e.data[1])

