let bu = 0;
function preventUnload(e: BeforeUnloadEvent) {
	e.preventDefault();
	return (e.returnValue = 'Page has not completed save, are you sure you want to exit?');
}
/** Helper function for blocking the unload event where necessary. */
export function blockUnload() {
	bu++ || document.addEventListener('beforeunload', preventUnload);
}
/** Helper function for unblocking the unload event. */
export function unblockUnload() {
	bu < 0 && (bu = 1);
	--bu || document.removeEventListener('beforeunload', preventUnload);
}
/** Helper function for creating chains where data is saved. */
export function createUnloadBlock(): (() => void) {
	blockUnload();
	return unblockUnload;
}
