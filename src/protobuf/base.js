/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots.Response || ($protobuf.roots.Response = {});

export const Enums = $root.Enums = (() => {

    /**
     * Properties of an Enums.
     * @exports IEnums
     * @interface IEnums
     */

    /**
     * Constructs a new Enums.
     * @exports Enums
     * @classdesc Represents an Enums.
     * @implements IEnums
     * @constructor
     * @param {IEnums=} [properties] Properties to set
     */
    function Enums(properties) {
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * Creates a new Enums instance using the specified properties.
     * @function create
     * @memberof Enums
     * @static
     * @param {IEnums=} [properties] Properties to set
     * @returns {Enums} Enums instance
     */
    Enums.create = function create(properties) {
        return new Enums(properties);
    };

    /**
     * Encodes the specified Enums message. Does not implicitly {@link Enums.verify|verify} messages.
     * @function encode
     * @memberof Enums
     * @static
     * @param {IEnums} message Enums message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Enums.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        return writer;
    };

    /**
     * Encodes the specified Enums message, length delimited. Does not implicitly {@link Enums.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Enums
     * @static
     * @param {IEnums} message Enums message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Enums.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an Enums message from the specified reader or buffer.
     * @function decode
     * @memberof Enums
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Enums} Enums
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Enums.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Enums();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes an Enums message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Enums
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Enums} Enums
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Enums.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an Enums message.
     * @function verify
     * @memberof Enums
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Enums.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        return null;
    };

    /**
     * Creates an Enums message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Enums
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Enums} Enums
     */
    Enums.fromObject = function fromObject(object) {
        if (object instanceof $root.Enums)
            return object;
        return new $root.Enums();
    };

    /**
     * Creates a plain object from an Enums message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Enums
     * @static
     * @param {Enums} message Enums
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Enums.toObject = function toObject() {
        return {};
    };

    /**
     * Converts this Enums to JSON.
     * @function toJSON
     * @memberof Enums
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Enums.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    /**
     * Status enum.
     * @name Enums.Status
     * @enum {string}
     * @property {number} Unknown=0 Unknown value
     * @property {number} Ongoing=1 Ongoing value
     * @property {number} Completed=2 Completed value
     * @property {number} Cancelled=3 Cancelled value
     * @property {number} Hiatus=4 Hiatus value
     */
    Enums.Status = (function() {
        const valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "Unknown"] = 0;
        values[valuesById[1] = "Ongoing"] = 1;
        values[valuesById[2] = "Completed"] = 2;
        values[valuesById[3] = "Cancelled"] = 3;
        values[valuesById[4] = "Hiatus"] = 4;
        return values;
    })();

    /**
     * ErrCode enum.
     * @name Enums.ErrCode
     * @enum {string}
     * @property {number} other=0 other value
     * @property {number} server_down=1 server_down value
     * @property {number} bad_request=2 bad_request value
     * @property {number} not_found=3 not_found value
     * @property {number} gone=4 gone value
     * @property {number} ddos=5 ddos value
     */
    Enums.ErrCode = (function() {
        const valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "other"] = 0;
        values[valuesById[1] = "server_down"] = 1;
        values[valuesById[2] = "bad_request"] = 2;
        values[valuesById[3] = "not_found"] = 3;
        values[valuesById[4] = "gone"] = 4;
        values[valuesById[5] = "ddos"] = 5;
        return values;
    })();

    /**
     * LangFlag enum.
     * @name Enums.LangFlag
     * @enum {string}
     * @property {number} none=0 none value
     * @property {number} gb=1 gb value
     * @property {number} jp=2 jp value
     * @property {number} pl=3 pl value
     * @property {number} rs=4 rs value
     * @property {number} nl=5 nl value
     * @property {number} it=6 it value
     * @property {number} ru=7 ru value
     * @property {number} de=8 de value
     * @property {number} hu=9 hu value
     * @property {number} fr=10 fr value
     * @property {number} fi=11 fi value
     * @property {number} vn=12 vn value
     * @property {number} gr=13 gr value
     * @property {number} bg=14 bg value
     * @property {number} es=15 es value
     * @property {number} br=16 br value
     * @property {number} pt=17 pt value
     * @property {number} se=18 se value
     * @property {number} sa=19 sa value
     * @property {number} dk=20 dk value
     * @property {number} cn=21 cn value
     * @property {number} bd=22 bd value
     * @property {number} ro=23 ro value
     * @property {number} cz=24 cz value
     * @property {number} mn=25 mn value
     * @property {number} tr=26 tr value
     * @property {number} id=27 id value
     * @property {number} kr=28 kr value
     * @property {number} mx=29 mx value
     * @property {number} ir=30 ir value
     * @property {number} my=31 my value
     * @property {number} th=32 th value
     * @property {number} ct=33 ct value
     * @property {number} ph=34 ph value
     * @property {number} hk=35 hk value
     * @property {number} ua=36 ua value
     * @property {number} mm=37 mm value
     * @property {number} lt=38 lt value
     */
    Enums.LangFlag = (function() {
        const valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "none"] = 0;
        values[valuesById[1] = "gb"] = 1;
        values[valuesById[2] = "jp"] = 2;
        values[valuesById[3] = "pl"] = 3;
        values[valuesById[4] = "rs"] = 4;
        values[valuesById[5] = "nl"] = 5;
        values[valuesById[6] = "it"] = 6;
        values[valuesById[7] = "ru"] = 7;
        values[valuesById[8] = "de"] = 8;
        values[valuesById[9] = "hu"] = 9;
        values[valuesById[10] = "fr"] = 10;
        values[valuesById[11] = "fi"] = 11;
        values[valuesById[12] = "vn"] = 12;
        values[valuesById[13] = "gr"] = 13;
        values[valuesById[14] = "bg"] = 14;
        values[valuesById[15] = "es"] = 15;
        values[valuesById[16] = "br"] = 16;
        values[valuesById[17] = "pt"] = 17;
        values[valuesById[18] = "se"] = 18;
        values[valuesById[19] = "sa"] = 19;
        values[valuesById[20] = "dk"] = 20;
        values[valuesById[21] = "cn"] = 21;
        values[valuesById[22] = "bd"] = 22;
        values[valuesById[23] = "ro"] = 23;
        values[valuesById[24] = "cz"] = 24;
        values[valuesById[25] = "mn"] = 25;
        values[valuesById[26] = "tr"] = 26;
        values[valuesById[27] = "id"] = 27;
        values[valuesById[28] = "kr"] = 28;
        values[valuesById[29] = "mx"] = 29;
        values[valuesById[30] = "ir"] = 30;
        values[valuesById[31] = "my"] = 31;
        values[valuesById[32] = "th"] = 32;
        values[valuesById[33] = "ct"] = 33;
        values[valuesById[34] = "ph"] = 34;
        values[valuesById[35] = "hk"] = 35;
        values[valuesById[36] = "ua"] = 36;
        values[valuesById[37] = "mm"] = 37;
        values[valuesById[38] = "lt"] = 38;
        return values;
    })();

    /**
     * RelatedBy enum.
     * @name Enums.RelatedBy
     * @enum {string}
     * @property {number} Self=0 Self value
     * @property {number} Prequel=1 Prequel value
     * @property {number} Sequel=2 Sequel value
     * @property {number} AdaptedFrom=3 AdaptedFrom value
     * @property {number} SpinOff=4 SpinOff value
     * @property {number} SideStory=5 SideStory value
     * @property {number} MainStory=6 MainStory value
     * @property {number} AltStory=7 AltStory value
     * @property {number} Doujinshi=8 Doujinshi value
     * @property {number} BasedOn=9 BasedOn value
     * @property {number} Coloured=10 Coloured value
     * @property {number} Monochrome=11 Monochrome value
     * @property {number} SharedUniverse=12 SharedUniverse value
     * @property {number} SameFranchise=13 SameFranchise value
     * @property {number} PreSerialisation=14 PreSerialisation value
     * @property {number} Serialisation=15 Serialisation value
     */
    Enums.RelatedBy = (function() {
        const valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "Self"] = 0;
        values[valuesById[1] = "Prequel"] = 1;
        values[valuesById[2] = "Sequel"] = 2;
        values[valuesById[3] = "AdaptedFrom"] = 3;
        values[valuesById[4] = "SpinOff"] = 4;
        values[valuesById[5] = "SideStory"] = 5;
        values[valuesById[6] = "MainStory"] = 6;
        values[valuesById[7] = "AltStory"] = 7;
        values[valuesById[8] = "Doujinshi"] = 8;
        values[valuesById[9] = "BasedOn"] = 9;
        values[valuesById[10] = "Coloured"] = 10;
        values[valuesById[11] = "Monochrome"] = 11;
        values[valuesById[12] = "SharedUniverse"] = 12;
        values[valuesById[13] = "SameFranchise"] = 13;
        values[valuesById[14] = "PreSerialisation"] = 14;
        values[valuesById[15] = "Serialisation"] = 15;
        return values;
    })();

    /**
     * Demographic enum.
     * @name Enums.Demographic
     * @enum {string}
     * @property {number} None=0 None value
     * @property {number} Shounen=1 Shounen value
     * @property {number} Shoujo=2 Shoujo value
     * @property {number} Seinen=3 Seinen value
     * @property {number} Jousei=4 Jousei value
     */
    Enums.Demographic = (function() {
        const valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "None"] = 0;
        values[valuesById[1] = "Shounen"] = 1;
        values[valuesById[2] = "Shoujo"] = 2;
        values[valuesById[3] = "Seinen"] = 3;
        values[valuesById[4] = "Jousei"] = 4;
        return values;
    })();

    /**
     * Genre enum.
     * @name Enums.Genre
     * @enum {string}
     * @property {number} None=0 None value
     * @property {number} FourKoma=1 FourKoma value
     * @property {number} Action=2 Action value
     * @property {number} Adventure=3 Adventure value
     * @property {number} AwardWinning=4 AwardWinning value
     * @property {number} Comedy=5 Comedy value
     * @property {number} Cooking=6 Cooking value
     * @property {number} Doujinshi=7 Doujinshi value
     * @property {number} Drama=8 Drama value
     * @property {number} Ecchi=9 Ecchi value
     * @property {number} Fantasy=10 Fantasy value
     * @property {number} Gyaru=11 Gyaru value
     * @property {number} Harem=12 Harem value
     * @property {number} Historical=13 Historical value
     * @property {number} Horror=14 Horror value
     * @property {number} MartialArts=16 MartialArts value
     * @property {number} Mecha=17 Mecha value
     * @property {number} Medical=18 Medical value
     * @property {number} Music=19 Music value
     * @property {number} Mystery=20 Mystery value
     * @property {number} Oneshot=21 Oneshot value
     * @property {number} Psychological=22 Psychological value
     * @property {number} Romance=23 Romance value
     * @property {number} SchoolLife=24 SchoolLife value
     * @property {number} SciFi=25 SciFi value
     * @property {number} ShoujoAi=28 ShoujoAi value
     * @property {number} ShounenAi=30 ShounenAi value
     * @property {number} SliceofLife=31 SliceofLife value
     * @property {number} Smut=32 Smut value
     * @property {number} Sports=33 Sports value
     * @property {number} Supernatural=34 Supernatural value
     * @property {number} Tragedy=35 Tragedy value
     * @property {number} LongStrip=36 LongStrip value
     * @property {number} Yaoi=37 Yaoi value
     * @property {number} Yuri=38 Yuri value
     * @property {number} VideoGames=40 VideoGames value
     * @property {number} Isekai=41 Isekai value
     * @property {number} Adaptation=42 Adaptation value
     * @property {number} Anthology=43 Anthology value
     * @property {number} WebComic=44 WebComic value
     * @property {number} FullColor=45 FullColor value
     * @property {number} UserCreated=46 UserCreated value
     * @property {number} OfficialColored=47 OfficialColored value
     * @property {number} FanColored=48 FanColored value
     * @property {number} Gore=49 Gore value
     * @property {number} SexualViolence=50 SexualViolence value
     * @property {number} Crime=51 Crime value
     * @property {number} MagicalGirls=52 MagicalGirls value
     * @property {number} Philosophical=53 Philosophical value
     * @property {number} Superhero=54 Superhero value
     * @property {number} Thriller=55 Thriller value
     * @property {number} Wuxia=56 Wuxia value
     * @property {number} Aliens=57 Aliens value
     * @property {number} Animals=58 Animals value
     * @property {number} Crossdressing=59 Crossdressing value
     * @property {number} Demons=60 Demons value
     * @property {number} Delinquents=61 Delinquents value
     * @property {number} Genderswap=62 Genderswap value
     * @property {number} Ghosts=63 Ghosts value
     * @property {number} MonsterGirls=64 MonsterGirls value
     * @property {number} Loli=65 Loli value
     * @property {number} Magic=66 Magic value
     * @property {number} Military=67 Military value
     * @property {number} Monsters=68 Monsters value
     * @property {number} Ninja=69 Ninja value
     * @property {number} OfficeWorkers=70 OfficeWorkers value
     * @property {number} Police=71 Police value
     * @property {number} PostApocalyptic=72 PostApocalyptic value
     * @property {number} Reincarnation=73 Reincarnation value
     * @property {number} ReverseHarem=74 ReverseHarem value
     * @property {number} Samurai=75 Samurai value
     * @property {number} Shota=76 Shota value
     * @property {number} Survival=77 Survival value
     * @property {number} TimeTravel=78 TimeTravel value
     * @property {number} Vampires=79 Vampires value
     * @property {number} TraditionalGames=80 TraditionalGames value
     * @property {number} VirtualReality=81 VirtualReality value
     * @property {number} Zombies=82 Zombies value
     * @property {number} Incest=83 Incest value
     */
    Enums.Genre = (function() {
        const valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "None"] = 0;
        values[valuesById[1] = "FourKoma"] = 1;
        values[valuesById[2] = "Action"] = 2;
        values[valuesById[3] = "Adventure"] = 3;
        values[valuesById[4] = "AwardWinning"] = 4;
        values[valuesById[5] = "Comedy"] = 5;
        values[valuesById[6] = "Cooking"] = 6;
        values[valuesById[7] = "Doujinshi"] = 7;
        values[valuesById[8] = "Drama"] = 8;
        values[valuesById[9] = "Ecchi"] = 9;
        values[valuesById[10] = "Fantasy"] = 10;
        values[valuesById[11] = "Gyaru"] = 11;
        values[valuesById[12] = "Harem"] = 12;
        values[valuesById[13] = "Historical"] = 13;
        values[valuesById[14] = "Horror"] = 14;
        values[valuesById[16] = "MartialArts"] = 16;
        values[valuesById[17] = "Mecha"] = 17;
        values[valuesById[18] = "Medical"] = 18;
        values[valuesById[19] = "Music"] = 19;
        values[valuesById[20] = "Mystery"] = 20;
        values[valuesById[21] = "Oneshot"] = 21;
        values[valuesById[22] = "Psychological"] = 22;
        values[valuesById[23] = "Romance"] = 23;
        values[valuesById[24] = "SchoolLife"] = 24;
        values[valuesById[25] = "SciFi"] = 25;
        values[valuesById[28] = "ShoujoAi"] = 28;
        values[valuesById[30] = "ShounenAi"] = 30;
        values[valuesById[31] = "SliceofLife"] = 31;
        values[valuesById[32] = "Smut"] = 32;
        values[valuesById[33] = "Sports"] = 33;
        values[valuesById[34] = "Supernatural"] = 34;
        values[valuesById[35] = "Tragedy"] = 35;
        values[valuesById[36] = "LongStrip"] = 36;
        values[valuesById[37] = "Yaoi"] = 37;
        values[valuesById[38] = "Yuri"] = 38;
        values[valuesById[40] = "VideoGames"] = 40;
        values[valuesById[41] = "Isekai"] = 41;
        values[valuesById[42] = "Adaptation"] = 42;
        values[valuesById[43] = "Anthology"] = 43;
        values[valuesById[44] = "WebComic"] = 44;
        values[valuesById[45] = "FullColor"] = 45;
        values[valuesById[46] = "UserCreated"] = 46;
        values[valuesById[47] = "OfficialColored"] = 47;
        values[valuesById[48] = "FanColored"] = 48;
        values[valuesById[49] = "Gore"] = 49;
        values[valuesById[50] = "SexualViolence"] = 50;
        values[valuesById[51] = "Crime"] = 51;
        values[valuesById[52] = "MagicalGirls"] = 52;
        values[valuesById[53] = "Philosophical"] = 53;
        values[valuesById[54] = "Superhero"] = 54;
        values[valuesById[55] = "Thriller"] = 55;
        values[valuesById[56] = "Wuxia"] = 56;
        values[valuesById[57] = "Aliens"] = 57;
        values[valuesById[58] = "Animals"] = 58;
        values[valuesById[59] = "Crossdressing"] = 59;
        values[valuesById[60] = "Demons"] = 60;
        values[valuesById[61] = "Delinquents"] = 61;
        values[valuesById[62] = "Genderswap"] = 62;
        values[valuesById[63] = "Ghosts"] = 63;
        values[valuesById[64] = "MonsterGirls"] = 64;
        values[valuesById[65] = "Loli"] = 65;
        values[valuesById[66] = "Magic"] = 66;
        values[valuesById[67] = "Military"] = 67;
        values[valuesById[68] = "Monsters"] = 68;
        values[valuesById[69] = "Ninja"] = 69;
        values[valuesById[70] = "OfficeWorkers"] = 70;
        values[valuesById[71] = "Police"] = 71;
        values[valuesById[72] = "PostApocalyptic"] = 72;
        values[valuesById[73] = "Reincarnation"] = 73;
        values[valuesById[74] = "ReverseHarem"] = 74;
        values[valuesById[75] = "Samurai"] = 75;
        values[valuesById[76] = "Shota"] = 76;
        values[valuesById[77] = "Survival"] = 77;
        values[valuesById[78] = "TimeTravel"] = 78;
        values[valuesById[79] = "Vampires"] = 79;
        values[valuesById[80] = "TraditionalGames"] = 80;
        values[valuesById[81] = "VirtualReality"] = 81;
        values[valuesById[82] = "Zombies"] = 82;
        values[valuesById[83] = "Incest"] = 83;
        return values;
    })();

    return Enums;
})();

export const Err = $root.Err = (() => {

    /**
     * Properties of an Err.
     * @exports IErr
     * @interface IErr
     * @property {Enums.ErrCode|null} [code] Err code
     * @property {string|null} [name] Err name
     * @property {string|null} [desc] Err desc
     */

    /**
     * Constructs a new Err.
     * @exports Err
     * @classdesc Represents an Err.
     * @implements IErr
     * @constructor
     * @param {IErr=} [properties] Properties to set
     */
    function Err(properties) {
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * Err code.
     * @member {Enums.ErrCode} code
     * @memberof Err
     * @instance
     */
    Err.prototype.code = 0;

    /**
     * Err name.
     * @member {string} name
     * @memberof Err
     * @instance
     */
    Err.prototype.name = "";

    /**
     * Err desc.
     * @member {string} desc
     * @memberof Err
     * @instance
     */
    Err.prototype.desc = "";

    /**
     * Creates a new Err instance using the specified properties.
     * @function create
     * @memberof Err
     * @static
     * @param {IErr=} [properties] Properties to set
     * @returns {Err} Err instance
     */
    Err.create = function create(properties) {
        return new Err(properties);
    };

    /**
     * Encodes the specified Err message. Does not implicitly {@link Err.verify|verify} messages.
     * @function encode
     * @memberof Err
     * @static
     * @param {IErr} message Err message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Err.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.code != null && message.hasOwnProperty("code"))
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.code);
        if (message.name != null && message.hasOwnProperty("name"))
            writer.uint32(/* id 2, wireType 2 =*/18).string(message.name);
        if (message.desc != null && message.hasOwnProperty("desc"))
            writer.uint32(/* id 3, wireType 2 =*/26).string(message.desc);
        return writer;
    };

    /**
     * Encodes the specified Err message, length delimited. Does not implicitly {@link Err.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Err
     * @static
     * @param {IErr} message Err message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Err.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an Err message from the specified reader or buffer.
     * @function decode
     * @memberof Err
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Err} Err
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Err.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Err();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.code = reader.int32();
                break;
            case 2:
                message.name = reader.string();
                break;
            case 3:
                message.desc = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes an Err message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Err
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Err} Err
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Err.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an Err message.
     * @function verify
     * @memberof Err
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Err.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.code != null && message.hasOwnProperty("code"))
            switch (message.code) {
            default:
                return "code: enum value expected";
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                break;
            }
        if (message.name != null && message.hasOwnProperty("name"))
            if (!$util.isString(message.name))
                return "name: string expected";
        if (message.desc != null && message.hasOwnProperty("desc"))
            if (!$util.isString(message.desc))
                return "desc: string expected";
        return null;
    };

    /**
     * Creates an Err message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Err
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Err} Err
     */
    Err.fromObject = function fromObject(object) {
        if (object instanceof $root.Err)
            return object;
        let message = new $root.Err();
        switch (object.code) {
        case "other":
        case 0:
            message.code = 0;
            break;
        case "server_down":
        case 1:
            message.code = 1;
            break;
        case "bad_request":
        case 2:
            message.code = 2;
            break;
        case "not_found":
        case 3:
            message.code = 3;
            break;
        case "gone":
        case 4:
            message.code = 4;
            break;
        case "ddos":
        case 5:
            message.code = 5;
            break;
        }
        if (object.name != null)
            message.name = String(object.name);
        if (object.desc != null)
            message.desc = String(object.desc);
        return message;
    };

    /**
     * Creates a plain object from an Err message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Err
     * @static
     * @param {Err} message Err
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Err.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.defaults) {
            object.code = options.enums === String ? "other" : 0;
            object.name = "";
            object.desc = "";
        }
        if (message.code != null && message.hasOwnProperty("code"))
            object.code = options.enums === String ? $root.Enums.ErrCode[message.code] : message.code;
        if (message.name != null && message.hasOwnProperty("name"))
            object.name = message.name;
        if (message.desc != null && message.hasOwnProperty("desc"))
            object.desc = message.desc;
        return object;
    };

    /**
     * Converts this Err to JSON.
     * @function toJSON
     * @memberof Err
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Err.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Err;
})();

export const Rating = $root.Rating = (() => {

    /**
     * Properties of a Rating.
     * @exports IRating
     * @interface IRating
     * @property {number|null} [votes_1] Rating votes_1
     * @property {number|null} [votes_2] Rating votes_2
     * @property {number|null} [votes_3] Rating votes_3
     * @property {number|null} [votes_4] Rating votes_4
     * @property {number|null} [votes_5] Rating votes_5
     * @property {number|null} [votes_6] Rating votes_6
     * @property {number|null} [votes_7] Rating votes_7
     * @property {number|null} [votes_8] Rating votes_8
     * @property {number|null} [votes_9] Rating votes_9
     * @property {number|null} [votes_10] Rating votes_10
     * @property {number|null} [bayesian] Rating bayesian
     * @property {number|null} [mean] Rating mean
     * @property {number|null} [votes] Rating votes
     * @property {number|null} [sumOfVotes] Rating sumOfVotes
     */

    /**
     * Constructs a new Rating.
     * @exports Rating
     * @classdesc Represents a Rating.
     * @implements IRating
     * @constructor
     * @param {IRating=} [properties] Properties to set
     */
    function Rating(properties) {
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * Rating votes_1.
     * @member {number} votes_1
     * @memberof Rating
     * @instance
     */
    Rating.prototype.votes_1 = 0;

    /**
     * Rating votes_2.
     * @member {number} votes_2
     * @memberof Rating
     * @instance
     */
    Rating.prototype.votes_2 = 0;

    /**
     * Rating votes_3.
     * @member {number} votes_3
     * @memberof Rating
     * @instance
     */
    Rating.prototype.votes_3 = 0;

    /**
     * Rating votes_4.
     * @member {number} votes_4
     * @memberof Rating
     * @instance
     */
    Rating.prototype.votes_4 = 0;

    /**
     * Rating votes_5.
     * @member {number} votes_5
     * @memberof Rating
     * @instance
     */
    Rating.prototype.votes_5 = 0;

    /**
     * Rating votes_6.
     * @member {number} votes_6
     * @memberof Rating
     * @instance
     */
    Rating.prototype.votes_6 = 0;

    /**
     * Rating votes_7.
     * @member {number} votes_7
     * @memberof Rating
     * @instance
     */
    Rating.prototype.votes_7 = 0;

    /**
     * Rating votes_8.
     * @member {number} votes_8
     * @memberof Rating
     * @instance
     */
    Rating.prototype.votes_8 = 0;

    /**
     * Rating votes_9.
     * @member {number} votes_9
     * @memberof Rating
     * @instance
     */
    Rating.prototype.votes_9 = 0;

    /**
     * Rating votes_10.
     * @member {number} votes_10
     * @memberof Rating
     * @instance
     */
    Rating.prototype.votes_10 = 0;

    /**
     * Rating bayesian.
     * @member {number} bayesian
     * @memberof Rating
     * @instance
     */
    Rating.prototype.bayesian = 0;

    /**
     * Rating mean.
     * @member {number} mean
     * @memberof Rating
     * @instance
     */
    Rating.prototype.mean = 0;

    /**
     * Rating votes.
     * @member {number} votes
     * @memberof Rating
     * @instance
     */
    Rating.prototype.votes = 0;

    /**
     * Rating sumOfVotes.
     * @member {number} sumOfVotes
     * @memberof Rating
     * @instance
     */
    Rating.prototype.sumOfVotes = 0;

    /**
     * Creates a new Rating instance using the specified properties.
     * @function create
     * @memberof Rating
     * @static
     * @param {IRating=} [properties] Properties to set
     * @returns {Rating} Rating instance
     */
    Rating.create = function create(properties) {
        return new Rating(properties);
    };

    /**
     * Encodes the specified Rating message. Does not implicitly {@link Rating.verify|verify} messages.
     * @function encode
     * @memberof Rating
     * @static
     * @param {IRating} message Rating message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Rating.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.votes_1 != null && message.hasOwnProperty("votes_1"))
            writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.votes_1);
        if (message.votes_2 != null && message.hasOwnProperty("votes_2"))
            writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.votes_2);
        if (message.votes_3 != null && message.hasOwnProperty("votes_3"))
            writer.uint32(/* id 3, wireType 0 =*/24).uint32(message.votes_3);
        if (message.votes_4 != null && message.hasOwnProperty("votes_4"))
            writer.uint32(/* id 4, wireType 0 =*/32).uint32(message.votes_4);
        if (message.votes_5 != null && message.hasOwnProperty("votes_5"))
            writer.uint32(/* id 5, wireType 0 =*/40).uint32(message.votes_5);
        if (message.votes_6 != null && message.hasOwnProperty("votes_6"))
            writer.uint32(/* id 6, wireType 0 =*/48).uint32(message.votes_6);
        if (message.votes_7 != null && message.hasOwnProperty("votes_7"))
            writer.uint32(/* id 7, wireType 0 =*/56).uint32(message.votes_7);
        if (message.votes_8 != null && message.hasOwnProperty("votes_8"))
            writer.uint32(/* id 8, wireType 0 =*/64).uint32(message.votes_8);
        if (message.votes_9 != null && message.hasOwnProperty("votes_9"))
            writer.uint32(/* id 9, wireType 0 =*/72).uint32(message.votes_9);
        if (message.votes_10 != null && message.hasOwnProperty("votes_10"))
            writer.uint32(/* id 10, wireType 0 =*/80).uint32(message.votes_10);
        if (message.bayesian != null && message.hasOwnProperty("bayesian"))
            writer.uint32(/* id 12, wireType 5 =*/101).float(message.bayesian);
        if (message.mean != null && message.hasOwnProperty("mean"))
            writer.uint32(/* id 13, wireType 5 =*/109).float(message.mean);
        if (message.votes != null && message.hasOwnProperty("votes"))
            writer.uint32(/* id 14, wireType 0 =*/112).uint32(message.votes);
        if (message.sumOfVotes != null && message.hasOwnProperty("sumOfVotes"))
            writer.uint32(/* id 15, wireType 0 =*/120).uint32(message.sumOfVotes);
        return writer;
    };

    /**
     * Encodes the specified Rating message, length delimited. Does not implicitly {@link Rating.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Rating
     * @static
     * @param {IRating} message Rating message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Rating.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Rating message from the specified reader or buffer.
     * @function decode
     * @memberof Rating
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Rating} Rating
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Rating.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Rating();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.votes_1 = reader.uint32();
                break;
            case 2:
                message.votes_2 = reader.uint32();
                break;
            case 3:
                message.votes_3 = reader.uint32();
                break;
            case 4:
                message.votes_4 = reader.uint32();
                break;
            case 5:
                message.votes_5 = reader.uint32();
                break;
            case 6:
                message.votes_6 = reader.uint32();
                break;
            case 7:
                message.votes_7 = reader.uint32();
                break;
            case 8:
                message.votes_8 = reader.uint32();
                break;
            case 9:
                message.votes_9 = reader.uint32();
                break;
            case 10:
                message.votes_10 = reader.uint32();
                break;
            case 12:
                message.bayesian = reader.float();
                break;
            case 13:
                message.mean = reader.float();
                break;
            case 14:
                message.votes = reader.uint32();
                break;
            case 15:
                message.sumOfVotes = reader.uint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a Rating message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Rating
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Rating} Rating
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Rating.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Rating message.
     * @function verify
     * @memberof Rating
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Rating.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.votes_1 != null && message.hasOwnProperty("votes_1"))
            if (!$util.isInteger(message.votes_1))
                return "votes_1: integer expected";
        if (message.votes_2 != null && message.hasOwnProperty("votes_2"))
            if (!$util.isInteger(message.votes_2))
                return "votes_2: integer expected";
        if (message.votes_3 != null && message.hasOwnProperty("votes_3"))
            if (!$util.isInteger(message.votes_3))
                return "votes_3: integer expected";
        if (message.votes_4 != null && message.hasOwnProperty("votes_4"))
            if (!$util.isInteger(message.votes_4))
                return "votes_4: integer expected";
        if (message.votes_5 != null && message.hasOwnProperty("votes_5"))
            if (!$util.isInteger(message.votes_5))
                return "votes_5: integer expected";
        if (message.votes_6 != null && message.hasOwnProperty("votes_6"))
            if (!$util.isInteger(message.votes_6))
                return "votes_6: integer expected";
        if (message.votes_7 != null && message.hasOwnProperty("votes_7"))
            if (!$util.isInteger(message.votes_7))
                return "votes_7: integer expected";
        if (message.votes_8 != null && message.hasOwnProperty("votes_8"))
            if (!$util.isInteger(message.votes_8))
                return "votes_8: integer expected";
        if (message.votes_9 != null && message.hasOwnProperty("votes_9"))
            if (!$util.isInteger(message.votes_9))
                return "votes_9: integer expected";
        if (message.votes_10 != null && message.hasOwnProperty("votes_10"))
            if (!$util.isInteger(message.votes_10))
                return "votes_10: integer expected";
        if (message.bayesian != null && message.hasOwnProperty("bayesian"))
            if (typeof message.bayesian !== "number")
                return "bayesian: number expected";
        if (message.mean != null && message.hasOwnProperty("mean"))
            if (typeof message.mean !== "number")
                return "mean: number expected";
        if (message.votes != null && message.hasOwnProperty("votes"))
            if (!$util.isInteger(message.votes))
                return "votes: integer expected";
        if (message.sumOfVotes != null && message.hasOwnProperty("sumOfVotes"))
            if (!$util.isInteger(message.sumOfVotes))
                return "sumOfVotes: integer expected";
        return null;
    };

    /**
     * Creates a Rating message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Rating
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Rating} Rating
     */
    Rating.fromObject = function fromObject(object) {
        if (object instanceof $root.Rating)
            return object;
        let message = new $root.Rating();
        if (object.votes_1 != null)
            message.votes_1 = object.votes_1 >>> 0;
        if (object.votes_2 != null)
            message.votes_2 = object.votes_2 >>> 0;
        if (object.votes_3 != null)
            message.votes_3 = object.votes_3 >>> 0;
        if (object.votes_4 != null)
            message.votes_4 = object.votes_4 >>> 0;
        if (object.votes_5 != null)
            message.votes_5 = object.votes_5 >>> 0;
        if (object.votes_6 != null)
            message.votes_6 = object.votes_6 >>> 0;
        if (object.votes_7 != null)
            message.votes_7 = object.votes_7 >>> 0;
        if (object.votes_8 != null)
            message.votes_8 = object.votes_8 >>> 0;
        if (object.votes_9 != null)
            message.votes_9 = object.votes_9 >>> 0;
        if (object.votes_10 != null)
            message.votes_10 = object.votes_10 >>> 0;
        if (object.bayesian != null)
            message.bayesian = Number(object.bayesian);
        if (object.mean != null)
            message.mean = Number(object.mean);
        if (object.votes != null)
            message.votes = object.votes >>> 0;
        if (object.sumOfVotes != null)
            message.sumOfVotes = object.sumOfVotes >>> 0;
        return message;
    };

    /**
     * Creates a plain object from a Rating message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Rating
     * @static
     * @param {Rating} message Rating
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Rating.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.defaults) {
            object.votes_1 = 0;
            object.votes_2 = 0;
            object.votes_3 = 0;
            object.votes_4 = 0;
            object.votes_5 = 0;
            object.votes_6 = 0;
            object.votes_7 = 0;
            object.votes_8 = 0;
            object.votes_9 = 0;
            object.votes_10 = 0;
            object.bayesian = 0;
            object.mean = 0;
            object.votes = 0;
            object.sumOfVotes = 0;
        }
        if (message.votes_1 != null && message.hasOwnProperty("votes_1"))
            object.votes_1 = message.votes_1;
        if (message.votes_2 != null && message.hasOwnProperty("votes_2"))
            object.votes_2 = message.votes_2;
        if (message.votes_3 != null && message.hasOwnProperty("votes_3"))
            object.votes_3 = message.votes_3;
        if (message.votes_4 != null && message.hasOwnProperty("votes_4"))
            object.votes_4 = message.votes_4;
        if (message.votes_5 != null && message.hasOwnProperty("votes_5"))
            object.votes_5 = message.votes_5;
        if (message.votes_6 != null && message.hasOwnProperty("votes_6"))
            object.votes_6 = message.votes_6;
        if (message.votes_7 != null && message.hasOwnProperty("votes_7"))
            object.votes_7 = message.votes_7;
        if (message.votes_8 != null && message.hasOwnProperty("votes_8"))
            object.votes_8 = message.votes_8;
        if (message.votes_9 != null && message.hasOwnProperty("votes_9"))
            object.votes_9 = message.votes_9;
        if (message.votes_10 != null && message.hasOwnProperty("votes_10"))
            object.votes_10 = message.votes_10;
        if (message.bayesian != null && message.hasOwnProperty("bayesian"))
            object.bayesian = options.json && !isFinite(message.bayesian) ? String(message.bayesian) : message.bayesian;
        if (message.mean != null && message.hasOwnProperty("mean"))
            object.mean = options.json && !isFinite(message.mean) ? String(message.mean) : message.mean;
        if (message.votes != null && message.hasOwnProperty("votes"))
            object.votes = message.votes;
        if (message.sumOfVotes != null && message.hasOwnProperty("sumOfVotes"))
            object.sumOfVotes = message.sumOfVotes;
        return object;
    };

    /**
     * Converts this Rating to JSON.
     * @function toJSON
     * @memberof Rating
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Rating.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Rating;
})();

export const Links = $root.Links = (() => {

    /**
     * Properties of a Links.
     * @exports ILinks
     * @interface ILinks
     * @property {number|null} [mangaUpdates] Links mangaUpdates
     * @property {number|null} [myAnimeList] Links myAnimeList
     * @property {string|null} [novelUpdates] Links novelUpdates
     * @property {string|null} [raw] Links raw
     * @property {string|null} [officialEng] Links officialEng
     * @property {string|null} [cdJapan] Links cdJapan
     * @property {string|null} [amazon] Links amazon
     * @property {string|null} [ebookjapan] Links ebookjapan
     * @property {string|null} [bookwalker] Links bookwalker
     * @property {string|null} [twitter] Links twitter
     * @property {string|null} [author] Links author
     */

    /**
     * Constructs a new Links.
     * @exports Links
     * @classdesc Represents a Links.
     * @implements ILinks
     * @constructor
     * @param {ILinks=} [properties] Properties to set
     */
    function Links(properties) {
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * Links mangaUpdates.
     * @member {number} mangaUpdates
     * @memberof Links
     * @instance
     */
    Links.prototype.mangaUpdates = 0;

    /**
     * Links myAnimeList.
     * @member {number} myAnimeList
     * @memberof Links
     * @instance
     */
    Links.prototype.myAnimeList = 0;

    /**
     * Links novelUpdates.
     * @member {string} novelUpdates
     * @memberof Links
     * @instance
     */
    Links.prototype.novelUpdates = "";

    /**
     * Links raw.
     * @member {string} raw
     * @memberof Links
     * @instance
     */
    Links.prototype.raw = "";

    /**
     * Links officialEng.
     * @member {string} officialEng
     * @memberof Links
     * @instance
     */
    Links.prototype.officialEng = "";

    /**
     * Links cdJapan.
     * @member {string} cdJapan
     * @memberof Links
     * @instance
     */
    Links.prototype.cdJapan = "";

    /**
     * Links amazon.
     * @member {string} amazon
     * @memberof Links
     * @instance
     */
    Links.prototype.amazon = "";

    /**
     * Links ebookjapan.
     * @member {string} ebookjapan
     * @memberof Links
     * @instance
     */
    Links.prototype.ebookjapan = "";

    /**
     * Links bookwalker.
     * @member {string} bookwalker
     * @memberof Links
     * @instance
     */
    Links.prototype.bookwalker = "";

    /**
     * Links twitter.
     * @member {string} twitter
     * @memberof Links
     * @instance
     */
    Links.prototype.twitter = "";

    /**
     * Links author.
     * @member {string} author
     * @memberof Links
     * @instance
     */
    Links.prototype.author = "";

    /**
     * Creates a new Links instance using the specified properties.
     * @function create
     * @memberof Links
     * @static
     * @param {ILinks=} [properties] Properties to set
     * @returns {Links} Links instance
     */
    Links.create = function create(properties) {
        return new Links(properties);
    };

    /**
     * Encodes the specified Links message. Does not implicitly {@link Links.verify|verify} messages.
     * @function encode
     * @memberof Links
     * @static
     * @param {ILinks} message Links message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Links.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.mangaUpdates != null && message.hasOwnProperty("mangaUpdates"))
            writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.mangaUpdates);
        if (message.myAnimeList != null && message.hasOwnProperty("myAnimeList"))
            writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.myAnimeList);
        if (message.novelUpdates != null && message.hasOwnProperty("novelUpdates"))
            writer.uint32(/* id 3, wireType 2 =*/26).string(message.novelUpdates);
        if (message.raw != null && message.hasOwnProperty("raw"))
            writer.uint32(/* id 4, wireType 2 =*/34).string(message.raw);
        if (message.officialEng != null && message.hasOwnProperty("officialEng"))
            writer.uint32(/* id 5, wireType 2 =*/42).string(message.officialEng);
        if (message.cdJapan != null && message.hasOwnProperty("cdJapan"))
            writer.uint32(/* id 6, wireType 2 =*/50).string(message.cdJapan);
        if (message.amazon != null && message.hasOwnProperty("amazon"))
            writer.uint32(/* id 7, wireType 2 =*/58).string(message.amazon);
        if (message.ebookjapan != null && message.hasOwnProperty("ebookjapan"))
            writer.uint32(/* id 8, wireType 2 =*/66).string(message.ebookjapan);
        if (message.bookwalker != null && message.hasOwnProperty("bookwalker"))
            writer.uint32(/* id 9, wireType 2 =*/74).string(message.bookwalker);
        if (message.twitter != null && message.hasOwnProperty("twitter"))
            writer.uint32(/* id 10, wireType 2 =*/82).string(message.twitter);
        if (message.author != null && message.hasOwnProperty("author"))
            writer.uint32(/* id 15, wireType 2 =*/122).string(message.author);
        return writer;
    };

    /**
     * Encodes the specified Links message, length delimited. Does not implicitly {@link Links.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Links
     * @static
     * @param {ILinks} message Links message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Links.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Links message from the specified reader or buffer.
     * @function decode
     * @memberof Links
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Links} Links
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Links.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Links();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.mangaUpdates = reader.uint32();
                break;
            case 2:
                message.myAnimeList = reader.uint32();
                break;
            case 3:
                message.novelUpdates = reader.string();
                break;
            case 4:
                message.raw = reader.string();
                break;
            case 5:
                message.officialEng = reader.string();
                break;
            case 6:
                message.cdJapan = reader.string();
                break;
            case 7:
                message.amazon = reader.string();
                break;
            case 8:
                message.ebookjapan = reader.string();
                break;
            case 9:
                message.bookwalker = reader.string();
                break;
            case 10:
                message.twitter = reader.string();
                break;
            case 15:
                message.author = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a Links message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Links
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Links} Links
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Links.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Links message.
     * @function verify
     * @memberof Links
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Links.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.mangaUpdates != null && message.hasOwnProperty("mangaUpdates"))
            if (!$util.isInteger(message.mangaUpdates))
                return "mangaUpdates: integer expected";
        if (message.myAnimeList != null && message.hasOwnProperty("myAnimeList"))
            if (!$util.isInteger(message.myAnimeList))
                return "myAnimeList: integer expected";
        if (message.novelUpdates != null && message.hasOwnProperty("novelUpdates"))
            if (!$util.isString(message.novelUpdates))
                return "novelUpdates: string expected";
        if (message.raw != null && message.hasOwnProperty("raw"))
            if (!$util.isString(message.raw))
                return "raw: string expected";
        if (message.officialEng != null && message.hasOwnProperty("officialEng"))
            if (!$util.isString(message.officialEng))
                return "officialEng: string expected";
        if (message.cdJapan != null && message.hasOwnProperty("cdJapan"))
            if (!$util.isString(message.cdJapan))
                return "cdJapan: string expected";
        if (message.amazon != null && message.hasOwnProperty("amazon"))
            if (!$util.isString(message.amazon))
                return "amazon: string expected";
        if (message.ebookjapan != null && message.hasOwnProperty("ebookjapan"))
            if (!$util.isString(message.ebookjapan))
                return "ebookjapan: string expected";
        if (message.bookwalker != null && message.hasOwnProperty("bookwalker"))
            if (!$util.isString(message.bookwalker))
                return "bookwalker: string expected";
        if (message.twitter != null && message.hasOwnProperty("twitter"))
            if (!$util.isString(message.twitter))
                return "twitter: string expected";
        if (message.author != null && message.hasOwnProperty("author"))
            if (!$util.isString(message.author))
                return "author: string expected";
        return null;
    };

    /**
     * Creates a Links message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Links
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Links} Links
     */
    Links.fromObject = function fromObject(object) {
        if (object instanceof $root.Links)
            return object;
        let message = new $root.Links();
        if (object.mangaUpdates != null)
            message.mangaUpdates = object.mangaUpdates >>> 0;
        if (object.myAnimeList != null)
            message.myAnimeList = object.myAnimeList >>> 0;
        if (object.novelUpdates != null)
            message.novelUpdates = String(object.novelUpdates);
        if (object.raw != null)
            message.raw = String(object.raw);
        if (object.officialEng != null)
            message.officialEng = String(object.officialEng);
        if (object.cdJapan != null)
            message.cdJapan = String(object.cdJapan);
        if (object.amazon != null)
            message.amazon = String(object.amazon);
        if (object.ebookjapan != null)
            message.ebookjapan = String(object.ebookjapan);
        if (object.bookwalker != null)
            message.bookwalker = String(object.bookwalker);
        if (object.twitter != null)
            message.twitter = String(object.twitter);
        if (object.author != null)
            message.author = String(object.author);
        return message;
    };

    /**
     * Creates a plain object from a Links message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Links
     * @static
     * @param {Links} message Links
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Links.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.defaults) {
            object.mangaUpdates = 0;
            object.myAnimeList = 0;
            object.novelUpdates = "";
            object.raw = "";
            object.officialEng = "";
            object.cdJapan = "";
            object.amazon = "";
            object.ebookjapan = "";
            object.bookwalker = "";
            object.twitter = "";
            object.author = "";
        }
        if (message.mangaUpdates != null && message.hasOwnProperty("mangaUpdates"))
            object.mangaUpdates = message.mangaUpdates;
        if (message.myAnimeList != null && message.hasOwnProperty("myAnimeList"))
            object.myAnimeList = message.myAnimeList;
        if (message.novelUpdates != null && message.hasOwnProperty("novelUpdates"))
            object.novelUpdates = message.novelUpdates;
        if (message.raw != null && message.hasOwnProperty("raw"))
            object.raw = message.raw;
        if (message.officialEng != null && message.hasOwnProperty("officialEng"))
            object.officialEng = message.officialEng;
        if (message.cdJapan != null && message.hasOwnProperty("cdJapan"))
            object.cdJapan = message.cdJapan;
        if (message.amazon != null && message.hasOwnProperty("amazon"))
            object.amazon = message.amazon;
        if (message.ebookjapan != null && message.hasOwnProperty("ebookjapan"))
            object.ebookjapan = message.ebookjapan;
        if (message.bookwalker != null && message.hasOwnProperty("bookwalker"))
            object.bookwalker = message.bookwalker;
        if (message.twitter != null && message.hasOwnProperty("twitter"))
            object.twitter = message.twitter;
        if (message.author != null && message.hasOwnProperty("author"))
            object.author = message.author;
        return object;
    };

    /**
     * Converts this Links to JSON.
     * @function toJSON
     * @memberof Links
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Links.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Links;
})();

export const Relation = $root.Relation = (() => {

    /**
     * Properties of a Relation.
     * @exports IRelation
     * @interface IRelation
     * @property {Enums.RelatedBy|null} [rel] Relation rel
     * @property {number|null} [id] Relation id
     */

    /**
     * Constructs a new Relation.
     * @exports Relation
     * @classdesc Represents a Relation.
     * @implements IRelation
     * @constructor
     * @param {IRelation=} [properties] Properties to set
     */
    function Relation(properties) {
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * Relation rel.
     * @member {Enums.RelatedBy} rel
     * @memberof Relation
     * @instance
     */
    Relation.prototype.rel = 0;

    /**
     * Relation id.
     * @member {number} id
     * @memberof Relation
     * @instance
     */
    Relation.prototype.id = 0;

    /**
     * Creates a new Relation instance using the specified properties.
     * @function create
     * @memberof Relation
     * @static
     * @param {IRelation=} [properties] Properties to set
     * @returns {Relation} Relation instance
     */
    Relation.create = function create(properties) {
        return new Relation(properties);
    };

    /**
     * Encodes the specified Relation message. Does not implicitly {@link Relation.verify|verify} messages.
     * @function encode
     * @memberof Relation
     * @static
     * @param {IRelation} message Relation message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Relation.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.rel != null && message.hasOwnProperty("rel"))
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.rel);
        if (message.id != null && message.hasOwnProperty("id"))
            writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.id);
        return writer;
    };

    /**
     * Encodes the specified Relation message, length delimited. Does not implicitly {@link Relation.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Relation
     * @static
     * @param {IRelation} message Relation message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Relation.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Relation message from the specified reader or buffer.
     * @function decode
     * @memberof Relation
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Relation} Relation
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Relation.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Relation();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.rel = reader.int32();
                break;
            case 2:
                message.id = reader.uint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a Relation message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Relation
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Relation} Relation
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Relation.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Relation message.
     * @function verify
     * @memberof Relation
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Relation.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.rel != null && message.hasOwnProperty("rel"))
            switch (message.rel) {
            default:
                return "rel: enum value expected";
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                break;
            }
        if (message.id != null && message.hasOwnProperty("id"))
            if (!$util.isInteger(message.id))
                return "id: integer expected";
        return null;
    };

    /**
     * Creates a Relation message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Relation
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Relation} Relation
     */
    Relation.fromObject = function fromObject(object) {
        if (object instanceof $root.Relation)
            return object;
        let message = new $root.Relation();
        switch (object.rel) {
        case "Self":
        case 0:
            message.rel = 0;
            break;
        case "Prequel":
        case 1:
            message.rel = 1;
            break;
        case "Sequel":
        case 2:
            message.rel = 2;
            break;
        case "AdaptedFrom":
        case 3:
            message.rel = 3;
            break;
        case "SpinOff":
        case 4:
            message.rel = 4;
            break;
        case "SideStory":
        case 5:
            message.rel = 5;
            break;
        case "MainStory":
        case 6:
            message.rel = 6;
            break;
        case "AltStory":
        case 7:
            message.rel = 7;
            break;
        case "Doujinshi":
        case 8:
            message.rel = 8;
            break;
        case "BasedOn":
        case 9:
            message.rel = 9;
            break;
        case "Coloured":
        case 10:
            message.rel = 10;
            break;
        case "Monochrome":
        case 11:
            message.rel = 11;
            break;
        case "SharedUniverse":
        case 12:
            message.rel = 12;
            break;
        case "SameFranchise":
        case 13:
            message.rel = 13;
            break;
        case "PreSerialisation":
        case 14:
            message.rel = 14;
            break;
        case "Serialisation":
        case 15:
            message.rel = 15;
            break;
        }
        if (object.id != null)
            message.id = object.id >>> 0;
        return message;
    };

    /**
     * Creates a plain object from a Relation message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Relation
     * @static
     * @param {Relation} message Relation
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Relation.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.defaults) {
            object.rel = options.enums === String ? "Self" : 0;
            object.id = 0;
        }
        if (message.rel != null && message.hasOwnProperty("rel"))
            object.rel = options.enums === String ? $root.Enums.RelatedBy[message.rel] : message.rel;
        if (message.id != null && message.hasOwnProperty("id"))
            object.id = message.id;
        return object;
    };

    /**
     * Converts this Relation to JSON.
     * @function toJSON
     * @memberof Relation
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Relation.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Relation;
})();

export const Title = $root.Title = (() => {

    /**
     * Properties of a Title.
     * @exports ITitle
     * @interface ITitle
     * @property {Array.<string>|null} [names] Title names
     * @property {string|null} [description] Title description
     * @property {boolean|null} [over_18] Title over_18
     * @property {Array.<Enums.Genre>|null} [genres] Title genres
     * @property {Enums.LangFlag|null} [originalLang] Title originalLang
     * @property {string|null} [endChapter] Title endChapter
     * @property {Enums.Status|null} [status] Title status
     * @property {ILinks|null} [links] Title links
     * @property {Array.<IRelation>|null} [relations] Title relations
     * @property {Array.<string>|null} [author] Title author
     * @property {Array.<string>|null} [artist] Title artist
     * @property {Array.<IChapter>|null} [chapters] Title chapters
     */

    /**
     * Constructs a new Title.
     * @exports Title
     * @classdesc Represents a Title.
     * @implements ITitle
     * @constructor
     * @param {ITitle=} [properties] Properties to set
     */
    function Title(properties) {
        this.names = [];
        this.genres = [];
        this.relations = [];
        this.author = [];
        this.artist = [];
        this.chapters = [];
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * Title names.
     * @member {Array.<string>} names
     * @memberof Title
     * @instance
     */
    Title.prototype.names = $util.emptyArray;

    /**
     * Title description.
     * @member {string} description
     * @memberof Title
     * @instance
     */
    Title.prototype.description = "";

    /**
     * Title over_18.
     * @member {boolean} over_18
     * @memberof Title
     * @instance
     */
    Title.prototype.over_18 = false;

    /**
     * Title genres.
     * @member {Array.<Enums.Genre>} genres
     * @memberof Title
     * @instance
     */
    Title.prototype.genres = $util.emptyArray;

    /**
     * Title originalLang.
     * @member {Enums.LangFlag} originalLang
     * @memberof Title
     * @instance
     */
    Title.prototype.originalLang = 0;

    /**
     * Title endChapter.
     * @member {string} endChapter
     * @memberof Title
     * @instance
     */
    Title.prototype.endChapter = "";

    /**
     * Title status.
     * @member {Enums.Status} status
     * @memberof Title
     * @instance
     */
    Title.prototype.status = 0;

    /**
     * Title links.
     * @member {ILinks|null|undefined} links
     * @memberof Title
     * @instance
     */
    Title.prototype.links = null;

    /**
     * Title relations.
     * @member {Array.<IRelation>} relations
     * @memberof Title
     * @instance
     */
    Title.prototype.relations = $util.emptyArray;

    /**
     * Title author.
     * @member {Array.<string>} author
     * @memberof Title
     * @instance
     */
    Title.prototype.author = $util.emptyArray;

    /**
     * Title artist.
     * @member {Array.<string>} artist
     * @memberof Title
     * @instance
     */
    Title.prototype.artist = $util.emptyArray;

    /**
     * Title chapters.
     * @member {Array.<IChapter>} chapters
     * @memberof Title
     * @instance
     */
    Title.prototype.chapters = $util.emptyArray;

    /**
     * Creates a new Title instance using the specified properties.
     * @function create
     * @memberof Title
     * @static
     * @param {ITitle=} [properties] Properties to set
     * @returns {Title} Title instance
     */
    Title.create = function create(properties) {
        return new Title(properties);
    };

    /**
     * Encodes the specified Title message. Does not implicitly {@link Title.verify|verify} messages.
     * @function encode
     * @memberof Title
     * @static
     * @param {ITitle} message Title message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Title.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.names != null && message.names.length)
            for (let i = 0; i < message.names.length; ++i)
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.names[i]);
        if (message.description != null && message.hasOwnProperty("description"))
            writer.uint32(/* id 2, wireType 2 =*/18).string(message.description);
        if (message.over_18 != null && message.hasOwnProperty("over_18"))
            writer.uint32(/* id 3, wireType 0 =*/24).bool(message.over_18);
        if (message.genres != null && message.genres.length) {
            writer.uint32(/* id 4, wireType 2 =*/34).fork();
            for (let i = 0; i < message.genres.length; ++i)
                writer.int32(message.genres[i]);
            writer.ldelim();
        }
        if (message.originalLang != null && message.hasOwnProperty("originalLang"))
            writer.uint32(/* id 5, wireType 0 =*/40).int32(message.originalLang);
        if (message.endChapter != null && message.hasOwnProperty("endChapter"))
            writer.uint32(/* id 6, wireType 2 =*/50).string(message.endChapter);
        if (message.status != null && message.hasOwnProperty("status"))
            writer.uint32(/* id 7, wireType 0 =*/56).int32(message.status);
        if (message.links != null && message.hasOwnProperty("links"))
            $root.Links.encode(message.links, writer.uint32(/* id 8, wireType 2 =*/66).fork()).ldelim();
        if (message.relations != null && message.relations.length)
            for (let i = 0; i < message.relations.length; ++i)
                $root.Relation.encode(message.relations[i], writer.uint32(/* id 9, wireType 2 =*/74).fork()).ldelim();
        if (message.author != null && message.author.length)
            for (let i = 0; i < message.author.length; ++i)
                writer.uint32(/* id 10, wireType 2 =*/82).string(message.author[i]);
        if (message.artist != null && message.artist.length)
            for (let i = 0; i < message.artist.length; ++i)
                writer.uint32(/* id 11, wireType 2 =*/90).string(message.artist[i]);
        if (message.chapters != null && message.chapters.length)
            for (let i = 0; i < message.chapters.length; ++i)
                $root.Chapter.encode(message.chapters[i], writer.uint32(/* id 15, wireType 2 =*/122).fork()).ldelim();
        return writer;
    };

    /**
     * Encodes the specified Title message, length delimited. Does not implicitly {@link Title.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Title
     * @static
     * @param {ITitle} message Title message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Title.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Title message from the specified reader or buffer.
     * @function decode
     * @memberof Title
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Title} Title
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Title.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Title();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.names && message.names.length))
                    message.names = [];
                message.names.push(reader.string());
                break;
            case 2:
                message.description = reader.string();
                break;
            case 3:
                message.over_18 = reader.bool();
                break;
            case 4:
                if (!(message.genres && message.genres.length))
                    message.genres = [];
                if ((tag & 7) === 2) {
                    let end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.genres.push(reader.int32());
                } else
                    message.genres.push(reader.int32());
                break;
            case 5:
                message.originalLang = reader.int32();
                break;
            case 6:
                message.endChapter = reader.string();
                break;
            case 7:
                message.status = reader.int32();
                break;
            case 8:
                message.links = $root.Links.decode(reader, reader.uint32());
                break;
            case 9:
                if (!(message.relations && message.relations.length))
                    message.relations = [];
                message.relations.push($root.Relation.decode(reader, reader.uint32()));
                break;
            case 10:
                if (!(message.author && message.author.length))
                    message.author = [];
                message.author.push(reader.string());
                break;
            case 11:
                if (!(message.artist && message.artist.length))
                    message.artist = [];
                message.artist.push(reader.string());
                break;
            case 15:
                if (!(message.chapters && message.chapters.length))
                    message.chapters = [];
                message.chapters.push($root.Chapter.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a Title message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Title
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Title} Title
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Title.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Title message.
     * @function verify
     * @memberof Title
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Title.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.names != null && message.hasOwnProperty("names")) {
            if (!Array.isArray(message.names))
                return "names: array expected";
            for (let i = 0; i < message.names.length; ++i)
                if (!$util.isString(message.names[i]))
                    return "names: string[] expected";
        }
        if (message.description != null && message.hasOwnProperty("description"))
            if (!$util.isString(message.description))
                return "description: string expected";
        if (message.over_18 != null && message.hasOwnProperty("over_18"))
            if (typeof message.over_18 !== "boolean")
                return "over_18: boolean expected";
        if (message.genres != null && message.hasOwnProperty("genres")) {
            if (!Array.isArray(message.genres))
                return "genres: array expected";
            for (let i = 0; i < message.genres.length; ++i)
                switch (message.genres[i]) {
                default:
                    return "genres: enum value[] expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 28:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                case 60:
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                case 68:
                case 69:
                case 70:
                case 71:
                case 72:
                case 73:
                case 74:
                case 75:
                case 76:
                case 77:
                case 78:
                case 79:
                case 80:
                case 81:
                case 82:
                case 83:
                    break;
                }
        }
        if (message.originalLang != null && message.hasOwnProperty("originalLang"))
            switch (message.originalLang) {
            default:
                return "originalLang: enum value expected";
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
                break;
            }
        if (message.endChapter != null && message.hasOwnProperty("endChapter"))
            if (!$util.isString(message.endChapter))
                return "endChapter: string expected";
        if (message.status != null && message.hasOwnProperty("status"))
            switch (message.status) {
            default:
                return "status: enum value expected";
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                break;
            }
        if (message.links != null && message.hasOwnProperty("links")) {
            let error = $root.Links.verify(message.links);
            if (error)
                return "links." + error;
        }
        if (message.relations != null && message.hasOwnProperty("relations")) {
            if (!Array.isArray(message.relations))
                return "relations: array expected";
            for (let i = 0; i < message.relations.length; ++i) {
                let error = $root.Relation.verify(message.relations[i]);
                if (error)
                    return "relations." + error;
            }
        }
        if (message.author != null && message.hasOwnProperty("author")) {
            if (!Array.isArray(message.author))
                return "author: array expected";
            for (let i = 0; i < message.author.length; ++i)
                if (!$util.isString(message.author[i]))
                    return "author: string[] expected";
        }
        if (message.artist != null && message.hasOwnProperty("artist")) {
            if (!Array.isArray(message.artist))
                return "artist: array expected";
            for (let i = 0; i < message.artist.length; ++i)
                if (!$util.isString(message.artist[i]))
                    return "artist: string[] expected";
        }
        if (message.chapters != null && message.hasOwnProperty("chapters")) {
            if (!Array.isArray(message.chapters))
                return "chapters: array expected";
            for (let i = 0; i < message.chapters.length; ++i) {
                let error = $root.Chapter.verify(message.chapters[i]);
                if (error)
                    return "chapters." + error;
            }
        }
        return null;
    };

    /**
     * Creates a Title message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Title
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Title} Title
     */
    Title.fromObject = function fromObject(object) {
        if (object instanceof $root.Title)
            return object;
        let message = new $root.Title();
        if (object.names) {
            if (!Array.isArray(object.names))
                throw TypeError(".Title.names: array expected");
            message.names = [];
            for (let i = 0; i < object.names.length; ++i)
                message.names[i] = String(object.names[i]);
        }
        if (object.description != null)
            message.description = String(object.description);
        if (object.over_18 != null)
            message.over_18 = Boolean(object.over_18);
        if (object.genres) {
            if (!Array.isArray(object.genres))
                throw TypeError(".Title.genres: array expected");
            message.genres = [];
            for (let i = 0; i < object.genres.length; ++i)
                switch (object.genres[i]) {
                default:
                case "None":
                case 0:
                    message.genres[i] = 0;
                    break;
                case "FourKoma":
                case 1:
                    message.genres[i] = 1;
                    break;
                case "Action":
                case 2:
                    message.genres[i] = 2;
                    break;
                case "Adventure":
                case 3:
                    message.genres[i] = 3;
                    break;
                case "AwardWinning":
                case 4:
                    message.genres[i] = 4;
                    break;
                case "Comedy":
                case 5:
                    message.genres[i] = 5;
                    break;
                case "Cooking":
                case 6:
                    message.genres[i] = 6;
                    break;
                case "Doujinshi":
                case 7:
                    message.genres[i] = 7;
                    break;
                case "Drama":
                case 8:
                    message.genres[i] = 8;
                    break;
                case "Ecchi":
                case 9:
                    message.genres[i] = 9;
                    break;
                case "Fantasy":
                case 10:
                    message.genres[i] = 10;
                    break;
                case "Gyaru":
                case 11:
                    message.genres[i] = 11;
                    break;
                case "Harem":
                case 12:
                    message.genres[i] = 12;
                    break;
                case "Historical":
                case 13:
                    message.genres[i] = 13;
                    break;
                case "Horror":
                case 14:
                    message.genres[i] = 14;
                    break;
                case "MartialArts":
                case 16:
                    message.genres[i] = 16;
                    break;
                case "Mecha":
                case 17:
                    message.genres[i] = 17;
                    break;
                case "Medical":
                case 18:
                    message.genres[i] = 18;
                    break;
                case "Music":
                case 19:
                    message.genres[i] = 19;
                    break;
                case "Mystery":
                case 20:
                    message.genres[i] = 20;
                    break;
                case "Oneshot":
                case 21:
                    message.genres[i] = 21;
                    break;
                case "Psychological":
                case 22:
                    message.genres[i] = 22;
                    break;
                case "Romance":
                case 23:
                    message.genres[i] = 23;
                    break;
                case "SchoolLife":
                case 24:
                    message.genres[i] = 24;
                    break;
                case "SciFi":
                case 25:
                    message.genres[i] = 25;
                    break;
                case "ShoujoAi":
                case 28:
                    message.genres[i] = 28;
                    break;
                case "ShounenAi":
                case 30:
                    message.genres[i] = 30;
                    break;
                case "SliceofLife":
                case 31:
                    message.genres[i] = 31;
                    break;
                case "Smut":
                case 32:
                    message.genres[i] = 32;
                    break;
                case "Sports":
                case 33:
                    message.genres[i] = 33;
                    break;
                case "Supernatural":
                case 34:
                    message.genres[i] = 34;
                    break;
                case "Tragedy":
                case 35:
                    message.genres[i] = 35;
                    break;
                case "LongStrip":
                case 36:
                    message.genres[i] = 36;
                    break;
                case "Yaoi":
                case 37:
                    message.genres[i] = 37;
                    break;
                case "Yuri":
                case 38:
                    message.genres[i] = 38;
                    break;
                case "VideoGames":
                case 40:
                    message.genres[i] = 40;
                    break;
                case "Isekai":
                case 41:
                    message.genres[i] = 41;
                    break;
                case "Adaptation":
                case 42:
                    message.genres[i] = 42;
                    break;
                case "Anthology":
                case 43:
                    message.genres[i] = 43;
                    break;
                case "WebComic":
                case 44:
                    message.genres[i] = 44;
                    break;
                case "FullColor":
                case 45:
                    message.genres[i] = 45;
                    break;
                case "UserCreated":
                case 46:
                    message.genres[i] = 46;
                    break;
                case "OfficialColored":
                case 47:
                    message.genres[i] = 47;
                    break;
                case "FanColored":
                case 48:
                    message.genres[i] = 48;
                    break;
                case "Gore":
                case 49:
                    message.genres[i] = 49;
                    break;
                case "SexualViolence":
                case 50:
                    message.genres[i] = 50;
                    break;
                case "Crime":
                case 51:
                    message.genres[i] = 51;
                    break;
                case "MagicalGirls":
                case 52:
                    message.genres[i] = 52;
                    break;
                case "Philosophical":
                case 53:
                    message.genres[i] = 53;
                    break;
                case "Superhero":
                case 54:
                    message.genres[i] = 54;
                    break;
                case "Thriller":
                case 55:
                    message.genres[i] = 55;
                    break;
                case "Wuxia":
                case 56:
                    message.genres[i] = 56;
                    break;
                case "Aliens":
                case 57:
                    message.genres[i] = 57;
                    break;
                case "Animals":
                case 58:
                    message.genres[i] = 58;
                    break;
                case "Crossdressing":
                case 59:
                    message.genres[i] = 59;
                    break;
                case "Demons":
                case 60:
                    message.genres[i] = 60;
                    break;
                case "Delinquents":
                case 61:
                    message.genres[i] = 61;
                    break;
                case "Genderswap":
                case 62:
                    message.genres[i] = 62;
                    break;
                case "Ghosts":
                case 63:
                    message.genres[i] = 63;
                    break;
                case "MonsterGirls":
                case 64:
                    message.genres[i] = 64;
                    break;
                case "Loli":
                case 65:
                    message.genres[i] = 65;
                    break;
                case "Magic":
                case 66:
                    message.genres[i] = 66;
                    break;
                case "Military":
                case 67:
                    message.genres[i] = 67;
                    break;
                case "Monsters":
                case 68:
                    message.genres[i] = 68;
                    break;
                case "Ninja":
                case 69:
                    message.genres[i] = 69;
                    break;
                case "OfficeWorkers":
                case 70:
                    message.genres[i] = 70;
                    break;
                case "Police":
                case 71:
                    message.genres[i] = 71;
                    break;
                case "PostApocalyptic":
                case 72:
                    message.genres[i] = 72;
                    break;
                case "Reincarnation":
                case 73:
                    message.genres[i] = 73;
                    break;
                case "ReverseHarem":
                case 74:
                    message.genres[i] = 74;
                    break;
                case "Samurai":
                case 75:
                    message.genres[i] = 75;
                    break;
                case "Shota":
                case 76:
                    message.genres[i] = 76;
                    break;
                case "Survival":
                case 77:
                    message.genres[i] = 77;
                    break;
                case "TimeTravel":
                case 78:
                    message.genres[i] = 78;
                    break;
                case "Vampires":
                case 79:
                    message.genres[i] = 79;
                    break;
                case "TraditionalGames":
                case 80:
                    message.genres[i] = 80;
                    break;
                case "VirtualReality":
                case 81:
                    message.genres[i] = 81;
                    break;
                case "Zombies":
                case 82:
                    message.genres[i] = 82;
                    break;
                case "Incest":
                case 83:
                    message.genres[i] = 83;
                    break;
                }
        }
        switch (object.originalLang) {
        case "none":
        case 0:
            message.originalLang = 0;
            break;
        case "gb":
        case 1:
            message.originalLang = 1;
            break;
        case "jp":
        case 2:
            message.originalLang = 2;
            break;
        case "pl":
        case 3:
            message.originalLang = 3;
            break;
        case "rs":
        case 4:
            message.originalLang = 4;
            break;
        case "nl":
        case 5:
            message.originalLang = 5;
            break;
        case "it":
        case 6:
            message.originalLang = 6;
            break;
        case "ru":
        case 7:
            message.originalLang = 7;
            break;
        case "de":
        case 8:
            message.originalLang = 8;
            break;
        case "hu":
        case 9:
            message.originalLang = 9;
            break;
        case "fr":
        case 10:
            message.originalLang = 10;
            break;
        case "fi":
        case 11:
            message.originalLang = 11;
            break;
        case "vn":
        case 12:
            message.originalLang = 12;
            break;
        case "gr":
        case 13:
            message.originalLang = 13;
            break;
        case "bg":
        case 14:
            message.originalLang = 14;
            break;
        case "es":
        case 15:
            message.originalLang = 15;
            break;
        case "br":
        case 16:
            message.originalLang = 16;
            break;
        case "pt":
        case 17:
            message.originalLang = 17;
            break;
        case "se":
        case 18:
            message.originalLang = 18;
            break;
        case "sa":
        case 19:
            message.originalLang = 19;
            break;
        case "dk":
        case 20:
            message.originalLang = 20;
            break;
        case "cn":
        case 21:
            message.originalLang = 21;
            break;
        case "bd":
        case 22:
            message.originalLang = 22;
            break;
        case "ro":
        case 23:
            message.originalLang = 23;
            break;
        case "cz":
        case 24:
            message.originalLang = 24;
            break;
        case "mn":
        case 25:
            message.originalLang = 25;
            break;
        case "tr":
        case 26:
            message.originalLang = 26;
            break;
        case "id":
        case 27:
            message.originalLang = 27;
            break;
        case "kr":
        case 28:
            message.originalLang = 28;
            break;
        case "mx":
        case 29:
            message.originalLang = 29;
            break;
        case "ir":
        case 30:
            message.originalLang = 30;
            break;
        case "my":
        case 31:
            message.originalLang = 31;
            break;
        case "th":
        case 32:
            message.originalLang = 32;
            break;
        case "ct":
        case 33:
            message.originalLang = 33;
            break;
        case "ph":
        case 34:
            message.originalLang = 34;
            break;
        case "hk":
        case 35:
            message.originalLang = 35;
            break;
        case "ua":
        case 36:
            message.originalLang = 36;
            break;
        case "mm":
        case 37:
            message.originalLang = 37;
            break;
        case "lt":
        case 38:
            message.originalLang = 38;
            break;
        }
        if (object.endChapter != null)
            message.endChapter = String(object.endChapter);
        switch (object.status) {
        case "Unknown":
        case 0:
            message.status = 0;
            break;
        case "Ongoing":
        case 1:
            message.status = 1;
            break;
        case "Completed":
        case 2:
            message.status = 2;
            break;
        case "Cancelled":
        case 3:
            message.status = 3;
            break;
        case "Hiatus":
        case 4:
            message.status = 4;
            break;
        }
        if (object.links != null) {
            if (typeof object.links !== "object")
                throw TypeError(".Title.links: object expected");
            message.links = $root.Links.fromObject(object.links);
        }
        if (object.relations) {
            if (!Array.isArray(object.relations))
                throw TypeError(".Title.relations: array expected");
            message.relations = [];
            for (let i = 0; i < object.relations.length; ++i) {
                if (typeof object.relations[i] !== "object")
                    throw TypeError(".Title.relations: object expected");
                message.relations[i] = $root.Relation.fromObject(object.relations[i]);
            }
        }
        if (object.author) {
            if (!Array.isArray(object.author))
                throw TypeError(".Title.author: array expected");
            message.author = [];
            for (let i = 0; i < object.author.length; ++i)
                message.author[i] = String(object.author[i]);
        }
        if (object.artist) {
            if (!Array.isArray(object.artist))
                throw TypeError(".Title.artist: array expected");
            message.artist = [];
            for (let i = 0; i < object.artist.length; ++i)
                message.artist[i] = String(object.artist[i]);
        }
        if (object.chapters) {
            if (!Array.isArray(object.chapters))
                throw TypeError(".Title.chapters: array expected");
            message.chapters = [];
            for (let i = 0; i < object.chapters.length; ++i) {
                if (typeof object.chapters[i] !== "object")
                    throw TypeError(".Title.chapters: object expected");
                message.chapters[i] = $root.Chapter.fromObject(object.chapters[i]);
            }
        }
        return message;
    };

    /**
     * Creates a plain object from a Title message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Title
     * @static
     * @param {Title} message Title
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Title.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.arrays || options.defaults) {
            object.names = [];
            object.genres = [];
            object.relations = [];
            object.author = [];
            object.artist = [];
            object.chapters = [];
        }
        if (options.defaults) {
            object.description = "";
            object.over_18 = false;
            object.originalLang = options.enums === String ? "none" : 0;
            object.endChapter = "";
            object.status = options.enums === String ? "Unknown" : 0;
            object.links = null;
        }
        if (message.names && message.names.length) {
            object.names = [];
            for (let j = 0; j < message.names.length; ++j)
                object.names[j] = message.names[j];
        }
        if (message.description != null && message.hasOwnProperty("description"))
            object.description = message.description;
        if (message.over_18 != null && message.hasOwnProperty("over_18"))
            object.over_18 = message.over_18;
        if (message.genres && message.genres.length) {
            object.genres = [];
            for (let j = 0; j < message.genres.length; ++j)
                object.genres[j] = options.enums === String ? $root.Enums.Genre[message.genres[j]] : message.genres[j];
        }
        if (message.originalLang != null && message.hasOwnProperty("originalLang"))
            object.originalLang = options.enums === String ? $root.Enums.LangFlag[message.originalLang] : message.originalLang;
        if (message.endChapter != null && message.hasOwnProperty("endChapter"))
            object.endChapter = message.endChapter;
        if (message.status != null && message.hasOwnProperty("status"))
            object.status = options.enums === String ? $root.Enums.Status[message.status] : message.status;
        if (message.links != null && message.hasOwnProperty("links"))
            object.links = $root.Links.toObject(message.links, options);
        if (message.relations && message.relations.length) {
            object.relations = [];
            for (let j = 0; j < message.relations.length; ++j)
                object.relations[j] = $root.Relation.toObject(message.relations[j], options);
        }
        if (message.author && message.author.length) {
            object.author = [];
            for (let j = 0; j < message.author.length; ++j)
                object.author[j] = message.author[j];
        }
        if (message.artist && message.artist.length) {
            object.artist = [];
            for (let j = 0; j < message.artist.length; ++j)
                object.artist[j] = message.artist[j];
        }
        if (message.chapters && message.chapters.length) {
            object.chapters = [];
            for (let j = 0; j < message.chapters.length; ++j)
                object.chapters[j] = $root.Chapter.toObject(message.chapters[j], options);
        }
        return object;
    };

    /**
     * Converts this Title to JSON.
     * @function toJSON
     * @memberof Title
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Title.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Title;
})();

export const ChapterData = $root.ChapterData = (() => {

    /**
     * Properties of a ChapterData.
     * @exports IChapterData
     * @interface IChapterData
     * @property {string|null} [hash] ChapterData hash
     * @property {number|null} [sMangadex] ChapterData sMangadex
     * @property {boolean|null} [cdndexOrElseData] ChapterData cdndexOrElseData
     * @property {string|null} [other] ChapterData other
     * @property {string|null} [prefix] ChapterData prefix
     * @property {number|null} [startFrom] ChapterData startFrom
     * @property {boolean|null} [ok] ChapterData ok
     */

    /**
     * Constructs a new ChapterData.
     * @exports ChapterData
     * @classdesc Represents a ChapterData.
     * @implements IChapterData
     * @constructor
     * @param {IChapterData=} [properties] Properties to set
     */
    function ChapterData(properties) {
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * ChapterData hash.
     * @member {string} hash
     * @memberof ChapterData
     * @instance
     */
    ChapterData.prototype.hash = "";

    /**
     * ChapterData sMangadex.
     * @member {number} sMangadex
     * @memberof ChapterData
     * @instance
     */
    ChapterData.prototype.sMangadex = 0;

    /**
     * ChapterData cdndexOrElseData.
     * @member {boolean} cdndexOrElseData
     * @memberof ChapterData
     * @instance
     */
    ChapterData.prototype.cdndexOrElseData = false;

    /**
     * ChapterData other.
     * @member {string} other
     * @memberof ChapterData
     * @instance
     */
    ChapterData.prototype.other = "";

    /**
     * ChapterData prefix.
     * @member {string} prefix
     * @memberof ChapterData
     * @instance
     */
    ChapterData.prototype.prefix = "";

    /**
     * ChapterData startFrom.
     * @member {number} startFrom
     * @memberof ChapterData
     * @instance
     */
    ChapterData.prototype.startFrom = 0;

    /**
     * ChapterData ok.
     * @member {boolean} ok
     * @memberof ChapterData
     * @instance
     */
    ChapterData.prototype.ok = false;

    // OneOf field names bound to virtual getters and setters
    let $oneOfFields;

    /**
     * ChapterData server.
     * @member {"sMangadex"|"cdndexOrElseData"|"other"|undefined} server
     * @memberof ChapterData
     * @instance
     */
    Object.defineProperty(ChapterData.prototype, "server", {
        get: $util.oneOfGetter($oneOfFields = ["sMangadex", "cdndexOrElseData", "other"]),
        set: $util.oneOfSetter($oneOfFields)
    });

    /**
     * Creates a new ChapterData instance using the specified properties.
     * @function create
     * @memberof ChapterData
     * @static
     * @param {IChapterData=} [properties] Properties to set
     * @returns {ChapterData} ChapterData instance
     */
    ChapterData.create = function create(properties) {
        return new ChapterData(properties);
    };

    /**
     * Encodes the specified ChapterData message. Does not implicitly {@link ChapterData.verify|verify} messages.
     * @function encode
     * @memberof ChapterData
     * @static
     * @param {IChapterData} message ChapterData message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ChapterData.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.hash != null && message.hasOwnProperty("hash"))
            writer.uint32(/* id 1, wireType 2 =*/10).string(message.hash);
        if (message.sMangadex != null && message.hasOwnProperty("sMangadex"))
            writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.sMangadex);
        if (message.cdndexOrElseData != null && message.hasOwnProperty("cdndexOrElseData"))
            writer.uint32(/* id 3, wireType 0 =*/24).bool(message.cdndexOrElseData);
        if (message.other != null && message.hasOwnProperty("other"))
            writer.uint32(/* id 4, wireType 2 =*/34).string(message.other);
        if (message.prefix != null && message.hasOwnProperty("prefix"))
            writer.uint32(/* id 5, wireType 2 =*/42).string(message.prefix);
        if (message.startFrom != null && message.hasOwnProperty("startFrom"))
            writer.uint32(/* id 6, wireType 0 =*/48).uint32(message.startFrom);
        if (message.ok != null && message.hasOwnProperty("ok"))
            writer.uint32(/* id 14, wireType 0 =*/112).bool(message.ok);
        return writer;
    };

    /**
     * Encodes the specified ChapterData message, length delimited. Does not implicitly {@link ChapterData.verify|verify} messages.
     * @function encodeDelimited
     * @memberof ChapterData
     * @static
     * @param {IChapterData} message ChapterData message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ChapterData.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a ChapterData message from the specified reader or buffer.
     * @function decode
     * @memberof ChapterData
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {ChapterData} ChapterData
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ChapterData.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.ChapterData();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.hash = reader.string();
                break;
            case 2:
                message.sMangadex = reader.uint32();
                break;
            case 3:
                message.cdndexOrElseData = reader.bool();
                break;
            case 4:
                message.other = reader.string();
                break;
            case 5:
                message.prefix = reader.string();
                break;
            case 6:
                message.startFrom = reader.uint32();
                break;
            case 14:
                message.ok = reader.bool();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a ChapterData message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof ChapterData
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {ChapterData} ChapterData
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ChapterData.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a ChapterData message.
     * @function verify
     * @memberof ChapterData
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ChapterData.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        let properties = {};
        if (message.hash != null && message.hasOwnProperty("hash"))
            if (!$util.isString(message.hash))
                return "hash: string expected";
        if (message.sMangadex != null && message.hasOwnProperty("sMangadex")) {
            properties.server = 1;
            if (!$util.isInteger(message.sMangadex))
                return "sMangadex: integer expected";
        }
        if (message.cdndexOrElseData != null && message.hasOwnProperty("cdndexOrElseData")) {
            if (properties.server === 1)
                return "server: multiple values";
            properties.server = 1;
            if (typeof message.cdndexOrElseData !== "boolean")
                return "cdndexOrElseData: boolean expected";
        }
        if (message.other != null && message.hasOwnProperty("other")) {
            if (properties.server === 1)
                return "server: multiple values";
            properties.server = 1;
            if (!$util.isString(message.other))
                return "other: string expected";
        }
        if (message.prefix != null && message.hasOwnProperty("prefix"))
            if (!$util.isString(message.prefix))
                return "prefix: string expected";
        if (message.startFrom != null && message.hasOwnProperty("startFrom"))
            if (!$util.isInteger(message.startFrom))
                return "startFrom: integer expected";
        if (message.ok != null && message.hasOwnProperty("ok"))
            if (typeof message.ok !== "boolean")
                return "ok: boolean expected";
        return null;
    };

    /**
     * Creates a ChapterData message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof ChapterData
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {ChapterData} ChapterData
     */
    ChapterData.fromObject = function fromObject(object) {
        if (object instanceof $root.ChapterData)
            return object;
        let message = new $root.ChapterData();
        if (object.hash != null)
            message.hash = String(object.hash);
        if (object.sMangadex != null)
            message.sMangadex = object.sMangadex >>> 0;
        if (object.cdndexOrElseData != null)
            message.cdndexOrElseData = Boolean(object.cdndexOrElseData);
        if (object.other != null)
            message.other = String(object.other);
        if (object.prefix != null)
            message.prefix = String(object.prefix);
        if (object.startFrom != null)
            message.startFrom = object.startFrom >>> 0;
        if (object.ok != null)
            message.ok = Boolean(object.ok);
        return message;
    };

    /**
     * Creates a plain object from a ChapterData message. Also converts values to other types if specified.
     * @function toObject
     * @memberof ChapterData
     * @static
     * @param {ChapterData} message ChapterData
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ChapterData.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.defaults) {
            object.hash = "";
            object.prefix = "";
            object.startFrom = 0;
            object.ok = false;
        }
        if (message.hash != null && message.hasOwnProperty("hash"))
            object.hash = message.hash;
        if (message.sMangadex != null && message.hasOwnProperty("sMangadex")) {
            object.sMangadex = message.sMangadex;
            if (options.oneofs)
                object.server = "sMangadex";
        }
        if (message.cdndexOrElseData != null && message.hasOwnProperty("cdndexOrElseData")) {
            object.cdndexOrElseData = message.cdndexOrElseData;
            if (options.oneofs)
                object.server = "cdndexOrElseData";
        }
        if (message.other != null && message.hasOwnProperty("other")) {
            object.other = message.other;
            if (options.oneofs)
                object.server = "other";
        }
        if (message.prefix != null && message.hasOwnProperty("prefix"))
            object.prefix = message.prefix;
        if (message.startFrom != null && message.hasOwnProperty("startFrom"))
            object.startFrom = message.startFrom;
        if (message.ok != null && message.hasOwnProperty("ok"))
            object.ok = message.ok;
        return object;
    };

    /**
     * Converts this ChapterData to JSON.
     * @function toJSON
     * @memberof ChapterData
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ChapterData.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    /**
     * ImageType enum.
     * @name ChapterData.ImageType
     * @enum {string}
     * @property {number} UNKNOWN=0 UNKNOWN value
     * @property {number} PNG=1 PNG value
     * @property {number} JPG=2 JPG value
     * @property {number} JPEG=3 JPEG value
     * @property {number} GIF=4 GIF value
     * @property {number} SVG=5 SVG value
     * @property {number} WEBP=6 WEBP value
     */
    ChapterData.ImageType = (function() {
        const valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "UNKNOWN"] = 0;
        values[valuesById[1] = "PNG"] = 1;
        values[valuesById[2] = "JPG"] = 2;
        values[valuesById[3] = "JPEG"] = 3;
        values[valuesById[4] = "GIF"] = 4;
        values[valuesById[5] = "SVG"] = 5;
        values[valuesById[6] = "WEBP"] = 6;
        return values;
    })();

    ChapterData.ImageValue = (function() {

        /**
         * Properties of an ImageValue.
         * @memberof ChapterData
         * @interface IImageValue
         * @property {number|null} [num] ImageValue num
         * @property {ChapterData.ImageType|null} [type] ImageValue type
         * @property {number|null} [width] ImageValue width
         * @property {number|null} [height] ImageValue height
         */

        /**
         * Constructs a new ImageValue.
         * @memberof ChapterData
         * @classdesc Represents an ImageValue.
         * @implements IImageValue
         * @constructor
         * @param {ChapterData.IImageValue=} [properties] Properties to set
         */
        function ImageValue(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * ImageValue num.
         * @member {number} num
         * @memberof ChapterData.ImageValue
         * @instance
         */
        ImageValue.prototype.num = 0;

        /**
         * ImageValue type.
         * @member {ChapterData.ImageType} type
         * @memberof ChapterData.ImageValue
         * @instance
         */
        ImageValue.prototype.type = 0;

        /**
         * ImageValue width.
         * @member {number} width
         * @memberof ChapterData.ImageValue
         * @instance
         */
        ImageValue.prototype.width = 0;

        /**
         * ImageValue height.
         * @member {number} height
         * @memberof ChapterData.ImageValue
         * @instance
         */
        ImageValue.prototype.height = 0;

        /**
         * Creates a new ImageValue instance using the specified properties.
         * @function create
         * @memberof ChapterData.ImageValue
         * @static
         * @param {ChapterData.IImageValue=} [properties] Properties to set
         * @returns {ChapterData.ImageValue} ImageValue instance
         */
        ImageValue.create = function create(properties) {
            return new ImageValue(properties);
        };

        /**
         * Encodes the specified ImageValue message. Does not implicitly {@link ChapterData.ImageValue.verify|verify} messages.
         * @function encode
         * @memberof ChapterData.ImageValue
         * @static
         * @param {ChapterData.IImageValue} message ImageValue message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ImageValue.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.num != null && message.hasOwnProperty("num"))
                writer.uint32(/* id 0, wireType 0 =*/0).uint32(message.num);
            if (message.type != null && message.hasOwnProperty("type"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.type);
            if (message.width != null && message.hasOwnProperty("width"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.width);
            if (message.height != null && message.hasOwnProperty("height"))
                writer.uint32(/* id 3, wireType 0 =*/24).uint32(message.height);
            return writer;
        };

        /**
         * Encodes the specified ImageValue message, length delimited. Does not implicitly {@link ChapterData.ImageValue.verify|verify} messages.
         * @function encodeDelimited
         * @memberof ChapterData.ImageValue
         * @static
         * @param {ChapterData.IImageValue} message ImageValue message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ImageValue.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an ImageValue message from the specified reader or buffer.
         * @function decode
         * @memberof ChapterData.ImageValue
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {ChapterData.ImageValue} ImageValue
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ImageValue.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.ChapterData.ImageValue();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 0:
                    message.num = reader.uint32();
                    break;
                case 1:
                    message.type = reader.int32();
                    break;
                case 2:
                    message.width = reader.uint32();
                    break;
                case 3:
                    message.height = reader.uint32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an ImageValue message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof ChapterData.ImageValue
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {ChapterData.ImageValue} ImageValue
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ImageValue.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an ImageValue message.
         * @function verify
         * @memberof ChapterData.ImageValue
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        ImageValue.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.num != null && message.hasOwnProperty("num"))
                if (!$util.isInteger(message.num))
                    return "num: integer expected";
            if (message.type != null && message.hasOwnProperty("type"))
                switch (message.type) {
                default:
                    return "type: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    break;
                }
            if (message.width != null && message.hasOwnProperty("width"))
                if (!$util.isInteger(message.width))
                    return "width: integer expected";
            if (message.height != null && message.hasOwnProperty("height"))
                if (!$util.isInteger(message.height))
                    return "height: integer expected";
            return null;
        };

        /**
         * Creates an ImageValue message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof ChapterData.ImageValue
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {ChapterData.ImageValue} ImageValue
         */
        ImageValue.fromObject = function fromObject(object) {
            if (object instanceof $root.ChapterData.ImageValue)
                return object;
            let message = new $root.ChapterData.ImageValue();
            if (object.num != null)
                message.num = object.num >>> 0;
            switch (object.type) {
            case "UNKNOWN":
            case 0:
                message.type = 0;
                break;
            case "PNG":
            case 1:
                message.type = 1;
                break;
            case "JPG":
            case 2:
                message.type = 2;
                break;
            case "JPEG":
            case 3:
                message.type = 3;
                break;
            case "GIF":
            case 4:
                message.type = 4;
                break;
            case "SVG":
            case 5:
                message.type = 5;
                break;
            case "WEBP":
            case 6:
                message.type = 6;
                break;
            }
            if (object.width != null)
                message.width = object.width >>> 0;
            if (object.height != null)
                message.height = object.height >>> 0;
            return message;
        };

        /**
         * Creates a plain object from an ImageValue message. Also converts values to other types if specified.
         * @function toObject
         * @memberof ChapterData.ImageValue
         * @static
         * @param {ChapterData.ImageValue} message ImageValue
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        ImageValue.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            let object = {};
            if (options.defaults) {
                object.num = 0;
                object.type = options.enums === String ? "UNKNOWN" : 0;
                object.width = 0;
                object.height = 0;
            }
            if (message.num != null && message.hasOwnProperty("num"))
                object.num = message.num;
            if (message.type != null && message.hasOwnProperty("type"))
                object.type = options.enums === String ? $root.ChapterData.ImageType[message.type] : message.type;
            if (message.width != null && message.hasOwnProperty("width"))
                object.width = message.width;
            if (message.height != null && message.hasOwnProperty("height"))
                object.height = message.height;
            return object;
        };

        /**
         * Converts this ImageValue to JSON.
         * @function toJSON
         * @memberof ChapterData.ImageValue
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        ImageValue.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return ImageValue;
    })();

    return ChapterData;
})();

export const Chapter = $root.Chapter = (() => {

    /**
     * Properties of a Chapter.
     * @exports IChapter
     * @interface IChapter
     * @property {number|null} [id] Chapter id
     * @property {number|null} [timestamp] Chapter timestamp
     * @property {Enums.LangFlag|null} [lang] Chapter lang
     * @property {Object.<string,string>|null} [user] Chapter user
     * @property {Object.<string,string>|null} [group] Chapter group
     * @property {string|null} [title] Chapter title
     * @property {string|null} [chapter] Chapter chapter
     * @property {number|null} [volume] Chapter volume
     */

    /**
     * Constructs a new Chapter.
     * @exports Chapter
     * @classdesc Represents a Chapter.
     * @implements IChapter
     * @constructor
     * @param {IChapter=} [properties] Properties to set
     */
    function Chapter(properties) {
        this.user = {};
        this.group = {};
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * Chapter id.
     * @member {number} id
     * @memberof Chapter
     * @instance
     */
    Chapter.prototype.id = 0;

    /**
     * Chapter timestamp.
     * @member {number} timestamp
     * @memberof Chapter
     * @instance
     */
    Chapter.prototype.timestamp = 0;

    /**
     * Chapter lang.
     * @member {Enums.LangFlag} lang
     * @memberof Chapter
     * @instance
     */
    Chapter.prototype.lang = 0;

    /**
     * Chapter user.
     * @member {Object.<string,string>} user
     * @memberof Chapter
     * @instance
     */
    Chapter.prototype.user = $util.emptyObject;

    /**
     * Chapter group.
     * @member {Object.<string,string>} group
     * @memberof Chapter
     * @instance
     */
    Chapter.prototype.group = $util.emptyObject;

    /**
     * Chapter title.
     * @member {string} title
     * @memberof Chapter
     * @instance
     */
    Chapter.prototype.title = "";

    /**
     * Chapter chapter.
     * @member {string} chapter
     * @memberof Chapter
     * @instance
     */
    Chapter.prototype.chapter = "";

    /**
     * Chapter volume.
     * @member {number} volume
     * @memberof Chapter
     * @instance
     */
    Chapter.prototype.volume = 0;

    /**
     * Creates a new Chapter instance using the specified properties.
     * @function create
     * @memberof Chapter
     * @static
     * @param {IChapter=} [properties] Properties to set
     * @returns {Chapter} Chapter instance
     */
    Chapter.create = function create(properties) {
        return new Chapter(properties);
    };

    /**
     * Encodes the specified Chapter message. Does not implicitly {@link Chapter.verify|verify} messages.
     * @function encode
     * @memberof Chapter
     * @static
     * @param {IChapter} message Chapter message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Chapter.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.id != null && message.hasOwnProperty("id"))
            writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.id);
        if (message.timestamp != null && message.hasOwnProperty("timestamp"))
            writer.uint32(/* id 2, wireType 5 =*/21).fixed32(message.timestamp);
        if (message.lang != null && message.hasOwnProperty("lang"))
            writer.uint32(/* id 3, wireType 0 =*/24).int32(message.lang);
        if (message.user != null && message.hasOwnProperty("user"))
            for (let keys = Object.keys(message.user), i = 0; i < keys.length; ++i)
                writer.uint32(/* id 4, wireType 2 =*/34).fork().uint32(/* id 1, wireType 0 =*/8).uint32(keys[i]).uint32(/* id 2, wireType 2 =*/18).string(message.user[keys[i]]).ldelim();
        if (message.group != null && message.hasOwnProperty("group"))
            for (let keys = Object.keys(message.group), i = 0; i < keys.length; ++i)
                writer.uint32(/* id 5, wireType 2 =*/42).fork().uint32(/* id 1, wireType 0 =*/8).uint32(keys[i]).uint32(/* id 2, wireType 2 =*/18).string(message.group[keys[i]]).ldelim();
        if (message.title != null && message.hasOwnProperty("title"))
            writer.uint32(/* id 6, wireType 2 =*/50).string(message.title);
        if (message.chapter != null && message.hasOwnProperty("chapter"))
            writer.uint32(/* id 7, wireType 2 =*/58).string(message.chapter);
        if (message.volume != null && message.hasOwnProperty("volume"))
            writer.uint32(/* id 8, wireType 0 =*/64).uint32(message.volume);
        return writer;
    };

    /**
     * Encodes the specified Chapter message, length delimited. Does not implicitly {@link Chapter.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Chapter
     * @static
     * @param {IChapter} message Chapter message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Chapter.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Chapter message from the specified reader or buffer.
     * @function decode
     * @memberof Chapter
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Chapter} Chapter
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Chapter.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Chapter(), key;
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.id = reader.uint32();
                break;
            case 2:
                message.timestamp = reader.fixed32();
                break;
            case 3:
                message.lang = reader.int32();
                break;
            case 4:
                reader.skip().pos++;
                if (message.user === $util.emptyObject)
                    message.user = {};
                key = reader.uint32();
                reader.pos++;
                message.user[key] = reader.string();
                break;
            case 5:
                reader.skip().pos++;
                if (message.group === $util.emptyObject)
                    message.group = {};
                key = reader.uint32();
                reader.pos++;
                message.group[key] = reader.string();
                break;
            case 6:
                message.title = reader.string();
                break;
            case 7:
                message.chapter = reader.string();
                break;
            case 8:
                message.volume = reader.uint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a Chapter message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Chapter
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Chapter} Chapter
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Chapter.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Chapter message.
     * @function verify
     * @memberof Chapter
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Chapter.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.id != null && message.hasOwnProperty("id"))
            if (!$util.isInteger(message.id))
                return "id: integer expected";
        if (message.timestamp != null && message.hasOwnProperty("timestamp"))
            if (!$util.isInteger(message.timestamp))
                return "timestamp: integer expected";
        if (message.lang != null && message.hasOwnProperty("lang"))
            switch (message.lang) {
            default:
                return "lang: enum value expected";
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
                break;
            }
        if (message.user != null && message.hasOwnProperty("user")) {
            if (!$util.isObject(message.user))
                return "user: object expected";
            let key = Object.keys(message.user);
            for (let i = 0; i < key.length; ++i) {
                if (!$util.key32Re.test(key[i]))
                    return "user: integer key{k:uint32} expected";
                if (!$util.isString(message.user[key[i]]))
                    return "user: string{k:uint32} expected";
            }
        }
        if (message.group != null && message.hasOwnProperty("group")) {
            if (!$util.isObject(message.group))
                return "group: object expected";
            let key = Object.keys(message.group);
            for (let i = 0; i < key.length; ++i) {
                if (!$util.key32Re.test(key[i]))
                    return "group: integer key{k:uint32} expected";
                if (!$util.isString(message.group[key[i]]))
                    return "group: string{k:uint32} expected";
            }
        }
        if (message.title != null && message.hasOwnProperty("title"))
            if (!$util.isString(message.title))
                return "title: string expected";
        if (message.chapter != null && message.hasOwnProperty("chapter"))
            if (!$util.isString(message.chapter))
                return "chapter: string expected";
        if (message.volume != null && message.hasOwnProperty("volume"))
            if (!$util.isInteger(message.volume))
                return "volume: integer expected";
        return null;
    };

    /**
     * Creates a Chapter message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Chapter
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Chapter} Chapter
     */
    Chapter.fromObject = function fromObject(object) {
        if (object instanceof $root.Chapter)
            return object;
        let message = new $root.Chapter();
        if (object.id != null)
            message.id = object.id >>> 0;
        if (object.timestamp != null)
            message.timestamp = object.timestamp >>> 0;
        switch (object.lang) {
        case "none":
        case 0:
            message.lang = 0;
            break;
        case "gb":
        case 1:
            message.lang = 1;
            break;
        case "jp":
        case 2:
            message.lang = 2;
            break;
        case "pl":
        case 3:
            message.lang = 3;
            break;
        case "rs":
        case 4:
            message.lang = 4;
            break;
        case "nl":
        case 5:
            message.lang = 5;
            break;
        case "it":
        case 6:
            message.lang = 6;
            break;
        case "ru":
        case 7:
            message.lang = 7;
            break;
        case "de":
        case 8:
            message.lang = 8;
            break;
        case "hu":
        case 9:
            message.lang = 9;
            break;
        case "fr":
        case 10:
            message.lang = 10;
            break;
        case "fi":
        case 11:
            message.lang = 11;
            break;
        case "vn":
        case 12:
            message.lang = 12;
            break;
        case "gr":
        case 13:
            message.lang = 13;
            break;
        case "bg":
        case 14:
            message.lang = 14;
            break;
        case "es":
        case 15:
            message.lang = 15;
            break;
        case "br":
        case 16:
            message.lang = 16;
            break;
        case "pt":
        case 17:
            message.lang = 17;
            break;
        case "se":
        case 18:
            message.lang = 18;
            break;
        case "sa":
        case 19:
            message.lang = 19;
            break;
        case "dk":
        case 20:
            message.lang = 20;
            break;
        case "cn":
        case 21:
            message.lang = 21;
            break;
        case "bd":
        case 22:
            message.lang = 22;
            break;
        case "ro":
        case 23:
            message.lang = 23;
            break;
        case "cz":
        case 24:
            message.lang = 24;
            break;
        case "mn":
        case 25:
            message.lang = 25;
            break;
        case "tr":
        case 26:
            message.lang = 26;
            break;
        case "id":
        case 27:
            message.lang = 27;
            break;
        case "kr":
        case 28:
            message.lang = 28;
            break;
        case "mx":
        case 29:
            message.lang = 29;
            break;
        case "ir":
        case 30:
            message.lang = 30;
            break;
        case "my":
        case 31:
            message.lang = 31;
            break;
        case "th":
        case 32:
            message.lang = 32;
            break;
        case "ct":
        case 33:
            message.lang = 33;
            break;
        case "ph":
        case 34:
            message.lang = 34;
            break;
        case "hk":
        case 35:
            message.lang = 35;
            break;
        case "ua":
        case 36:
            message.lang = 36;
            break;
        case "mm":
        case 37:
            message.lang = 37;
            break;
        case "lt":
        case 38:
            message.lang = 38;
            break;
        }
        if (object.user) {
            if (typeof object.user !== "object")
                throw TypeError(".Chapter.user: object expected");
            message.user = {};
            for (let keys = Object.keys(object.user), i = 0; i < keys.length; ++i)
                message.user[keys[i]] = String(object.user[keys[i]]);
        }
        if (object.group) {
            if (typeof object.group !== "object")
                throw TypeError(".Chapter.group: object expected");
            message.group = {};
            for (let keys = Object.keys(object.group), i = 0; i < keys.length; ++i)
                message.group[keys[i]] = String(object.group[keys[i]]);
        }
        if (object.title != null)
            message.title = String(object.title);
        if (object.chapter != null)
            message.chapter = String(object.chapter);
        if (object.volume != null)
            message.volume = object.volume >>> 0;
        return message;
    };

    /**
     * Creates a plain object from a Chapter message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Chapter
     * @static
     * @param {Chapter} message Chapter
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Chapter.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.objects || options.defaults) {
            object.user = {};
            object.group = {};
        }
        if (options.defaults) {
            object.id = 0;
            object.timestamp = 0;
            object.lang = options.enums === String ? "none" : 0;
            object.title = "";
            object.chapter = "";
            object.volume = 0;
        }
        if (message.id != null && message.hasOwnProperty("id"))
            object.id = message.id;
        if (message.timestamp != null && message.hasOwnProperty("timestamp"))
            object.timestamp = message.timestamp;
        if (message.lang != null && message.hasOwnProperty("lang"))
            object.lang = options.enums === String ? $root.Enums.LangFlag[message.lang] : message.lang;
        let keys2;
        if (message.user && (keys2 = Object.keys(message.user)).length) {
            object.user = {};
            for (let j = 0; j < keys2.length; ++j)
                object.user[keys2[j]] = message.user[keys2[j]];
        }
        if (message.group && (keys2 = Object.keys(message.group)).length) {
            object.group = {};
            for (let j = 0; j < keys2.length; ++j)
                object.group[keys2[j]] = message.group[keys2[j]];
        }
        if (message.title != null && message.hasOwnProperty("title"))
            object.title = message.title;
        if (message.chapter != null && message.hasOwnProperty("chapter"))
            object.chapter = message.chapter;
        if (message.volume != null && message.hasOwnProperty("volume"))
            object.volume = message.volume;
        return object;
    };

    /**
     * Converts this Chapter to JSON.
     * @function toJSON
     * @memberof Chapter
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Chapter.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Chapter;
})();

export const Comment = $root.Comment = (() => {

    /**
     * Properties of a Comment.
     * @exports IComment
     * @interface IComment
     * @property {number|null} [id] Comment id
     * @property {number|null} [timestamp] Comment timestamp
     * @property {string|null} [content] Comment content
     */

    /**
     * Constructs a new Comment.
     * @exports Comment
     * @classdesc Represents a Comment.
     * @implements IComment
     * @constructor
     * @param {IComment=} [properties] Properties to set
     */
    function Comment(properties) {
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * Comment id.
     * @member {number} id
     * @memberof Comment
     * @instance
     */
    Comment.prototype.id = 0;

    /**
     * Comment timestamp.
     * @member {number} timestamp
     * @memberof Comment
     * @instance
     */
    Comment.prototype.timestamp = 0;

    /**
     * Comment content.
     * @member {string} content
     * @memberof Comment
     * @instance
     */
    Comment.prototype.content = "";

    /**
     * Creates a new Comment instance using the specified properties.
     * @function create
     * @memberof Comment
     * @static
     * @param {IComment=} [properties] Properties to set
     * @returns {Comment} Comment instance
     */
    Comment.create = function create(properties) {
        return new Comment(properties);
    };

    /**
     * Encodes the specified Comment message. Does not implicitly {@link Comment.verify|verify} messages.
     * @function encode
     * @memberof Comment
     * @static
     * @param {IComment} message Comment message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Comment.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.id != null && message.hasOwnProperty("id"))
            writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.id);
        if (message.timestamp != null && message.hasOwnProperty("timestamp"))
            writer.uint32(/* id 2, wireType 5 =*/21).fixed32(message.timestamp);
        if (message.content != null && message.hasOwnProperty("content"))
            writer.uint32(/* id 3, wireType 2 =*/26).string(message.content);
        return writer;
    };

    /**
     * Encodes the specified Comment message, length delimited. Does not implicitly {@link Comment.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Comment
     * @static
     * @param {IComment} message Comment message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Comment.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Comment message from the specified reader or buffer.
     * @function decode
     * @memberof Comment
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Comment} Comment
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Comment.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Comment();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.id = reader.uint32();
                break;
            case 2:
                message.timestamp = reader.fixed32();
                break;
            case 3:
                message.content = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a Comment message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Comment
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Comment} Comment
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Comment.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Comment message.
     * @function verify
     * @memberof Comment
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Comment.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.id != null && message.hasOwnProperty("id"))
            if (!$util.isInteger(message.id))
                return "id: integer expected";
        if (message.timestamp != null && message.hasOwnProperty("timestamp"))
            if (!$util.isInteger(message.timestamp))
                return "timestamp: integer expected";
        if (message.content != null && message.hasOwnProperty("content"))
            if (!$util.isString(message.content))
                return "content: string expected";
        return null;
    };

    /**
     * Creates a Comment message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Comment
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Comment} Comment
     */
    Comment.fromObject = function fromObject(object) {
        if (object instanceof $root.Comment)
            return object;
        let message = new $root.Comment();
        if (object.id != null)
            message.id = object.id >>> 0;
        if (object.timestamp != null)
            message.timestamp = object.timestamp >>> 0;
        if (object.content != null)
            message.content = String(object.content);
        return message;
    };

    /**
     * Creates a plain object from a Comment message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Comment
     * @static
     * @param {Comment} message Comment
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Comment.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.defaults) {
            object.id = 0;
            object.timestamp = 0;
            object.content = "";
        }
        if (message.id != null && message.hasOwnProperty("id"))
            object.id = message.id;
        if (message.timestamp != null && message.hasOwnProperty("timestamp"))
            object.timestamp = message.timestamp;
        if (message.content != null && message.hasOwnProperty("content"))
            object.content = message.content;
        return object;
    };

    /**
     * Converts this Comment to JSON.
     * @function toJSON
     * @memberof Comment
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Comment.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Comment;
})();

export const Thread = $root.Thread = (() => {

    /**
     * Properties of a Thread.
     * @exports IThread
     * @interface IThread
     * @property {Array.<IComment>|null} [comments] Thread comments
     */

    /**
     * Constructs a new Thread.
     * @exports Thread
     * @classdesc Represents a Thread.
     * @implements IThread
     * @constructor
     * @param {IThread=} [properties] Properties to set
     */
    function Thread(properties) {
        this.comments = [];
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * Thread comments.
     * @member {Array.<IComment>} comments
     * @memberof Thread
     * @instance
     */
    Thread.prototype.comments = $util.emptyArray;

    /**
     * Creates a new Thread instance using the specified properties.
     * @function create
     * @memberof Thread
     * @static
     * @param {IThread=} [properties] Properties to set
     * @returns {Thread} Thread instance
     */
    Thread.create = function create(properties) {
        return new Thread(properties);
    };

    /**
     * Encodes the specified Thread message. Does not implicitly {@link Thread.verify|verify} messages.
     * @function encode
     * @memberof Thread
     * @static
     * @param {IThread} message Thread message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Thread.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.comments != null && message.comments.length)
            for (let i = 0; i < message.comments.length; ++i)
                $root.Comment.encode(message.comments[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
        return writer;
    };

    /**
     * Encodes the specified Thread message, length delimited. Does not implicitly {@link Thread.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Thread
     * @static
     * @param {IThread} message Thread message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Thread.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Thread message from the specified reader or buffer.
     * @function decode
     * @memberof Thread
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Thread} Thread
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Thread.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Thread();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.comments && message.comments.length))
                    message.comments = [];
                message.comments.push($root.Comment.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a Thread message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Thread
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Thread} Thread
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Thread.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Thread message.
     * @function verify
     * @memberof Thread
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Thread.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.comments != null && message.hasOwnProperty("comments")) {
            if (!Array.isArray(message.comments))
                return "comments: array expected";
            for (let i = 0; i < message.comments.length; ++i) {
                let error = $root.Comment.verify(message.comments[i]);
                if (error)
                    return "comments." + error;
            }
        }
        return null;
    };

    /**
     * Creates a Thread message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Thread
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Thread} Thread
     */
    Thread.fromObject = function fromObject(object) {
        if (object instanceof $root.Thread)
            return object;
        let message = new $root.Thread();
        if (object.comments) {
            if (!Array.isArray(object.comments))
                throw TypeError(".Thread.comments: array expected");
            message.comments = [];
            for (let i = 0; i < object.comments.length; ++i) {
                if (typeof object.comments[i] !== "object")
                    throw TypeError(".Thread.comments: object expected");
                message.comments[i] = $root.Comment.fromObject(object.comments[i]);
            }
        }
        return message;
    };

    /**
     * Creates a plain object from a Thread message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Thread
     * @static
     * @param {Thread} message Thread
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Thread.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (options.arrays || options.defaults)
            object.comments = [];
        if (message.comments && message.comments.length) {
            object.comments = [];
            for (let j = 0; j < message.comments.length; ++j)
                object.comments[j] = $root.Comment.toObject(message.comments[j], options);
        }
        return object;
    };

    /**
     * Converts this Thread to JSON.
     * @function toJSON
     * @memberof Thread
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Thread.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Thread;
})();

export const Response = $root.Response = (() => {

    /**
     * Properties of a Response.
     * @exports IResponse
     * @interface IResponse
     * @property {IErr|null} [err] Response err
     * @property {ITitle|null} [title] Response title
     * @property {IChapter|null} [chapter] Response chapter
     * @property {IChapterData|null} [chapterData] Response chapterData
     * @property {IThread|null} [thread] Response thread
     * @property {string|null} [unknownJson] Response unknownJson
     */

    /**
     * Constructs a new Response.
     * @exports Response
     * @classdesc Represents a Response.
     * @implements IResponse
     * @constructor
     * @param {IResponse=} [properties] Properties to set
     */
    function Response(properties) {
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * Response err.
     * @member {IErr|null|undefined} err
     * @memberof Response
     * @instance
     */
    Response.prototype.err = null;

    /**
     * Response title.
     * @member {ITitle|null|undefined} title
     * @memberof Response
     * @instance
     */
    Response.prototype.title = null;

    /**
     * Response chapter.
     * @member {IChapter|null|undefined} chapter
     * @memberof Response
     * @instance
     */
    Response.prototype.chapter = null;

    /**
     * Response chapterData.
     * @member {IChapterData|null|undefined} chapterData
     * @memberof Response
     * @instance
     */
    Response.prototype.chapterData = null;

    /**
     * Response thread.
     * @member {IThread|null|undefined} thread
     * @memberof Response
     * @instance
     */
    Response.prototype.thread = null;

    /**
     * Response unknownJson.
     * @member {string} unknownJson
     * @memberof Response
     * @instance
     */
    Response.prototype.unknownJson = "";

    // OneOf field names bound to virtual getters and setters
    let $oneOfFields;

    /**
     * Response resp.
     * @member {"err"|"title"|"chapter"|"chapterData"|"thread"|"unknownJson"|undefined} resp
     * @memberof Response
     * @instance
     */
    Object.defineProperty(Response.prototype, "resp", {
        get: $util.oneOfGetter($oneOfFields = ["err", "title", "chapter", "chapterData", "thread", "unknownJson"]),
        set: $util.oneOfSetter($oneOfFields)
    });

    /**
     * Creates a new Response instance using the specified properties.
     * @function create
     * @memberof Response
     * @static
     * @param {IResponse=} [properties] Properties to set
     * @returns {Response} Response instance
     */
    Response.create = function create(properties) {
        return new Response(properties);
    };

    /**
     * Encodes the specified Response message. Does not implicitly {@link Response.verify|verify} messages.
     * @function encode
     * @memberof Response
     * @static
     * @param {IResponse} message Response message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Response.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.err != null && message.hasOwnProperty("err"))
            $root.Err.encode(message.err, writer.uint32(/* id 0, wireType 2 =*/2).fork()).ldelim();
        if (message.title != null && message.hasOwnProperty("title"))
            $root.Title.encode(message.title, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
        if (message.chapter != null && message.hasOwnProperty("chapter"))
            $root.Chapter.encode(message.chapter, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
        if (message.chapterData != null && message.hasOwnProperty("chapterData"))
            $root.ChapterData.encode(message.chapterData, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
        if (message.thread != null && message.hasOwnProperty("thread"))
            $root.Thread.encode(message.thread, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
        if (message.unknownJson != null && message.hasOwnProperty("unknownJson"))
            writer.uint32(/* id 6, wireType 2 =*/50).string(message.unknownJson);
        return writer;
    };

    /**
     * Encodes the specified Response message, length delimited. Does not implicitly {@link Response.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Response
     * @static
     * @param {IResponse} message Response message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Response.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Response message from the specified reader or buffer.
     * @function decode
     * @memberof Response
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Response} Response
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Response.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        let end = length === undefined ? reader.len : reader.pos + length, message = new $root.Response();
        while (reader.pos < end) {
            let tag = reader.uint32();
            switch (tag >>> 3) {
            case 0:
                message.err = $root.Err.decode(reader, reader.uint32());
                break;
            case 1:
                message.title = $root.Title.decode(reader, reader.uint32());
                break;
            case 2:
                message.chapter = $root.Chapter.decode(reader, reader.uint32());
                break;
            case 3:
                message.chapterData = $root.ChapterData.decode(reader, reader.uint32());
                break;
            case 5:
                message.thread = $root.Thread.decode(reader, reader.uint32());
                break;
            case 6:
                message.unknownJson = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a Response message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Response
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Response} Response
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Response.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Response message.
     * @function verify
     * @memberof Response
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Response.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        let properties = {};
        if (message.err != null && message.hasOwnProperty("err")) {
            properties.resp = 1;
            {
                let error = $root.Err.verify(message.err);
                if (error)
                    return "err." + error;
            }
        }
        if (message.title != null && message.hasOwnProperty("title")) {
            if (properties.resp === 1)
                return "resp: multiple values";
            properties.resp = 1;
            {
                let error = $root.Title.verify(message.title);
                if (error)
                    return "title." + error;
            }
        }
        if (message.chapter != null && message.hasOwnProperty("chapter")) {
            if (properties.resp === 1)
                return "resp: multiple values";
            properties.resp = 1;
            {
                let error = $root.Chapter.verify(message.chapter);
                if (error)
                    return "chapter." + error;
            }
        }
        if (message.chapterData != null && message.hasOwnProperty("chapterData")) {
            if (properties.resp === 1)
                return "resp: multiple values";
            properties.resp = 1;
            {
                let error = $root.ChapterData.verify(message.chapterData);
                if (error)
                    return "chapterData." + error;
            }
        }
        if (message.thread != null && message.hasOwnProperty("thread")) {
            if (properties.resp === 1)
                return "resp: multiple values";
            properties.resp = 1;
            {
                let error = $root.Thread.verify(message.thread);
                if (error)
                    return "thread." + error;
            }
        }
        if (message.unknownJson != null && message.hasOwnProperty("unknownJson")) {
            if (properties.resp === 1)
                return "resp: multiple values";
            properties.resp = 1;
            if (!$util.isString(message.unknownJson))
                return "unknownJson: string expected";
        }
        return null;
    };

    /**
     * Creates a Response message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Response
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Response} Response
     */
    Response.fromObject = function fromObject(object) {
        if (object instanceof $root.Response)
            return object;
        let message = new $root.Response();
        if (object.err != null) {
            if (typeof object.err !== "object")
                throw TypeError(".Response.err: object expected");
            message.err = $root.Err.fromObject(object.err);
        }
        if (object.title != null) {
            if (typeof object.title !== "object")
                throw TypeError(".Response.title: object expected");
            message.title = $root.Title.fromObject(object.title);
        }
        if (object.chapter != null) {
            if (typeof object.chapter !== "object")
                throw TypeError(".Response.chapter: object expected");
            message.chapter = $root.Chapter.fromObject(object.chapter);
        }
        if (object.chapterData != null) {
            if (typeof object.chapterData !== "object")
                throw TypeError(".Response.chapterData: object expected");
            message.chapterData = $root.ChapterData.fromObject(object.chapterData);
        }
        if (object.thread != null) {
            if (typeof object.thread !== "object")
                throw TypeError(".Response.thread: object expected");
            message.thread = $root.Thread.fromObject(object.thread);
        }
        if (object.unknownJson != null)
            message.unknownJson = String(object.unknownJson);
        return message;
    };

    /**
     * Creates a plain object from a Response message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Response
     * @static
     * @param {Response} message Response
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Response.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        let object = {};
        if (message.err != null && message.hasOwnProperty("err")) {
            object.err = $root.Err.toObject(message.err, options);
            if (options.oneofs)
                object.resp = "err";
        }
        if (message.title != null && message.hasOwnProperty("title")) {
            object.title = $root.Title.toObject(message.title, options);
            if (options.oneofs)
                object.resp = "title";
        }
        if (message.chapter != null && message.hasOwnProperty("chapter")) {
            object.chapter = $root.Chapter.toObject(message.chapter, options);
            if (options.oneofs)
                object.resp = "chapter";
        }
        if (message.chapterData != null && message.hasOwnProperty("chapterData")) {
            object.chapterData = $root.ChapterData.toObject(message.chapterData, options);
            if (options.oneofs)
                object.resp = "chapterData";
        }
        if (message.thread != null && message.hasOwnProperty("thread")) {
            object.thread = $root.Thread.toObject(message.thread, options);
            if (options.oneofs)
                object.resp = "thread";
        }
        if (message.unknownJson != null && message.hasOwnProperty("unknownJson")) {
            object.unknownJson = message.unknownJson;
            if (options.oneofs)
                object.resp = "unknownJson";
        }
        return object;
    };

    /**
     * Converts this Response to JSON.
     * @function toJSON
     * @memberof Response
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Response.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Response;
})();

export { $root as default };
