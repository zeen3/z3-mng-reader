
use std::io::Write;
use std::borrow::Cow;
use std::collections::HashMap;
use quick_protobuf::{MessageRead, MessageWrite, BytesReader, Writer, Result};
use quick_protobuf::sizeofs::*;
use super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Error<'a> {
    pub code: enums::ErrCode,
    pub name: Cow<'a, str>,
    pub desc: Cow<'a, str>,
}

impl<'a> MessageRead<'a> for Error<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.code = r.read_enum(bytes)?,
                Ok(18) => msg.name = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(26) => msg.desc = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Error<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_varint(*(&self.code) as u64)
        + 1 + sizeof_len((&self.name).len())
        + 1 + sizeof_len((&self.desc).len())
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(8, |w| w.write_enum(*&self.code as i32))?;
        w.write_with_tag(18, |w| w.write_string(&**&self.name))?;
        w.write_with_tag(26, |w| w.write_string(&**&self.desc))?;
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Rating {
    pub votes: u32,
    pub votes_1: u32,
    pub votes_2: u32,
    pub votes_3: u32,
    pub votes_4: u32,
    pub votes_5: u32,
    pub votes_6: u32,
    pub votes_7: u32,
    pub votes_8: u32,
    pub votes_9: u32,
    pub votes_10: u32,
    pub bayesian: f32,
    pub mean: f32,
    pub sum_of_votes: u32,
}

impl<'a> MessageRead<'a> for Rating {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Rating {
            bayesian: ::std::f32::NAN,
            mean: ::std::f32::NAN,
            ..Self::default()
        };
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(0) => msg.votes = r.read_uint32(bytes)?,
                Ok(8) => msg.votes_1 = r.read_uint32(bytes)?,
                Ok(16) => msg.votes_2 = r.read_uint32(bytes)?,
                Ok(24) => msg.votes_3 = r.read_uint32(bytes)?,
                Ok(32) => msg.votes_4 = r.read_uint32(bytes)?,
                Ok(40) => msg.votes_5 = r.read_uint32(bytes)?,
                Ok(48) => msg.votes_6 = r.read_uint32(bytes)?,
                Ok(56) => msg.votes_7 = r.read_uint32(bytes)?,
                Ok(64) => msg.votes_8 = r.read_uint32(bytes)?,
                Ok(72) => msg.votes_9 = r.read_uint32(bytes)?,
                Ok(80) => msg.votes_10 = r.read_uint32(bytes)?,
                Ok(109) => msg.bayesian = r.read_float(bytes)?,
                Ok(117) => msg.mean = r.read_float(bytes)?,
                Ok(120) => msg.sum_of_votes = r.read_uint32(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Rating {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_varint(*(&self.votes) as u64)
        + 1 + sizeof_varint(*(&self.votes_1) as u64)
        + 1 + sizeof_varint(*(&self.votes_2) as u64)
        + 1 + sizeof_varint(*(&self.votes_3) as u64)
        + 1 + sizeof_varint(*(&self.votes_4) as u64)
        + 1 + sizeof_varint(*(&self.votes_5) as u64)
        + 1 + sizeof_varint(*(&self.votes_6) as u64)
        + 1 + sizeof_varint(*(&self.votes_7) as u64)
        + 1 + sizeof_varint(*(&self.votes_8) as u64)
        + 1 + sizeof_varint(*(&self.votes_9) as u64)
        + 1 + sizeof_varint(*(&self.votes_10) as u64)
        + 1 + 4
        + 1 + 4
        + 1 + sizeof_varint(*(&self.sum_of_votes) as u64)
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(0, |w| w.write_uint32(*&self.votes))?;
        w.write_with_tag(8, |w| w.write_uint32(*&self.votes_1))?;
        w.write_with_tag(16, |w| w.write_uint32(*&self.votes_2))?;
        w.write_with_tag(24, |w| w.write_uint32(*&self.votes_3))?;
        w.write_with_tag(32, |w| w.write_uint32(*&self.votes_4))?;
        w.write_with_tag(40, |w| w.write_uint32(*&self.votes_5))?;
        w.write_with_tag(48, |w| w.write_uint32(*&self.votes_6))?;
        w.write_with_tag(56, |w| w.write_uint32(*&self.votes_7))?;
        w.write_with_tag(64, |w| w.write_uint32(*&self.votes_8))?;
        w.write_with_tag(72, |w| w.write_uint32(*&self.votes_9))?;
        w.write_with_tag(80, |w| w.write_uint32(*&self.votes_10))?;
        w.write_with_tag(109, |w| w.write_float(*&self.bayesian))?;
        w.write_with_tag(117, |w| w.write_float(*&self.mean))?;
        w.write_with_tag(120, |w| w.write_uint32(*&self.sum_of_votes))?;
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Links<'a> {
    pub manga_updates: u32,
    pub my_anime_list: u32,
    pub novel_updates: Cow<'a, str>,
    pub raw: Cow<'a, str>,
    pub official_eng: Cow<'a, str>,
    pub cd_japan: Cow<'a, str>,
    pub amazon: Cow<'a, str>,
    pub ebookjapan: Cow<'a, str>,
    pub bookwalker: Cow<'a, str>,
    pub twitter: Cow<'a, str>,
    pub author: Cow<'a, str>,
}

impl<'a> MessageRead<'a> for Links<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.manga_updates = r.read_uint32(bytes)?,
                Ok(16) => msg.my_anime_list = r.read_uint32(bytes)?,
                Ok(26) => msg.novel_updates = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(34) => msg.raw = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(42) => msg.official_eng = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(50) => msg.cd_japan = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(58) => msg.amazon = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(66) => msg.ebookjapan = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(74) => msg.bookwalker = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(82) => msg.twitter = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(122) => msg.author = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Links<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_varint(*(&self.manga_updates) as u64)
        + 1 + sizeof_varint(*(&self.my_anime_list) as u64)
        + 1 + sizeof_len((&self.novel_updates).len())
        + 1 + sizeof_len((&self.raw).len())
        + 1 + sizeof_len((&self.official_eng).len())
        + 1 + sizeof_len((&self.cd_japan).len())
        + 1 + sizeof_len((&self.amazon).len())
        + 1 + sizeof_len((&self.ebookjapan).len())
        + 1 + sizeof_len((&self.bookwalker).len())
        + 1 + sizeof_len((&self.twitter).len())
        + 1 + sizeof_len((&self.author).len())
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(8, |w| w.write_uint32(*&self.manga_updates))?;
        w.write_with_tag(16, |w| w.write_uint32(*&self.my_anime_list))?;
        w.write_with_tag(26, |w| w.write_string(&**&self.novel_updates))?;
        w.write_with_tag(34, |w| w.write_string(&**&self.raw))?;
        w.write_with_tag(42, |w| w.write_string(&**&self.official_eng))?;
        w.write_with_tag(50, |w| w.write_string(&**&self.cd_japan))?;
        w.write_with_tag(58, |w| w.write_string(&**&self.amazon))?;
        w.write_with_tag(66, |w| w.write_string(&**&self.ebookjapan))?;
        w.write_with_tag(74, |w| w.write_string(&**&self.bookwalker))?;
        w.write_with_tag(82, |w| w.write_string(&**&self.twitter))?;
        w.write_with_tag(122, |w| w.write_string(&**&self.author))?;
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Relation {
    pub id: Vec<u32>,
}

impl<'a> MessageRead<'a> for Relation {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.id = r.read_packed(bytes, |r, bytes| r.read_uint32(bytes))?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Relation {
    fn get_size(&self) -> usize {
        0
        + if self.id.is_empty() { 0 } else { 1 + sizeof_len(self.id.iter().map(|s| sizeof_varint(*(s) as u64)).sum::<usize>()) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_packed_with_tag(10, &self.id, |w, m| w.write_uint32(*m), &|m| sizeof_varint(*(m) as u64))?;
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Title<'a> {
    pub names: Vec<Cow<'a, str>>,
    pub description: Option<Cow<'a, str>>,
    pub over_18: Option<bool>,
    pub genres: Vec<enums::Genre>,
    pub original_lang: enums::LangFlag,
    pub end_chapter: Option<Cow<'a, str>>,
    pub status: enums::Status,
    pub links: Links<'a>,
    pub relations: HashMap<enums::RelatedBy, Relation>,
    pub author: Vec<Cow<'a, str>>,
    pub artist: Vec<Cow<'a, str>>,
    pub chapters: Vec<Chapter<'a>>,
}

impl<'a> MessageRead<'a> for Title<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.names.push(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(18) => msg.description = Some(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(24) => msg.over_18 = Some(r.read_bool(bytes)?),
                Ok(34) => msg.genres = r.read_packed(bytes, |r, bytes| r.read_enum(bytes))?,
                Ok(40) => msg.original_lang = r.read_enum(bytes)?,
                Ok(50) => msg.end_chapter = Some(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(56) => msg.status = r.read_enum(bytes)?,
                Ok(66) => msg.links = r.read_message::<Links>(bytes)?,
                Ok(74) => {
                    let (key, value) = r.read_map(bytes, |r, bytes| r.read_enum(bytes), |r, bytes| r.read_message::<Relation>(bytes))?;
                    msg.relations.insert(key, value);
                }
                Ok(82) => msg.author.push(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(90) => msg.artist.push(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(122) => msg.chapters.push(r.read_message::<Chapter>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Title<'a> {
    fn get_size(&self) -> usize {
        0
        + self.names.iter().map(|s| 1 + sizeof_len((s).len())).sum::<usize>()
        + self.description.as_ref().map_or(0, |m| 1 + sizeof_len((m).len()))
        + self.over_18.as_ref().map_or(0, |m| 1 + sizeof_varint(*(m) as u64))
        + if self.genres.is_empty() { 0 } else { 1 + sizeof_len(self.genres.iter().map(|s| sizeof_varint(*(s) as u64)).sum::<usize>()) }
        + 1 + sizeof_varint(*(&self.original_lang) as u64)
        + self.end_chapter.as_ref().map_or(0, |m| 1 + sizeof_len((m).len()))
        + 1 + sizeof_varint(*(&self.status) as u64)
        + 1 + sizeof_len((&self.links).get_size())
        + self.relations.iter().map(|(k, v)| 1 + sizeof_len(2 + sizeof_varint(*(k) as u64) + sizeof_len((v).get_size()))).sum::<usize>()
        + self.author.iter().map(|s| 1 + sizeof_len((s).len())).sum::<usize>()
        + self.artist.iter().map(|s| 1 + sizeof_len((s).len())).sum::<usize>()
        + self.chapters.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.names { w.write_with_tag(10, |w| w.write_string(&**s))?; }
        if let Some(ref s) = self.description { w.write_with_tag(18, |w| w.write_string(&**s))?; }
        if let Some(ref s) = self.over_18 { w.write_with_tag(24, |w| w.write_bool(*s))?; }
        w.write_packed_with_tag(34, &self.genres, |w, m| w.write_enum(*m as i32), &|m| sizeof_varint(*(m) as u64))?;
        w.write_with_tag(40, |w| w.write_enum(*&self.original_lang as i32))?;
        if let Some(ref s) = self.end_chapter { w.write_with_tag(50, |w| w.write_string(&**s))?; }
        w.write_with_tag(56, |w| w.write_enum(*&self.status as i32))?;
        w.write_with_tag(66, |w| w.write_message(&self.links))?;
        for (k, v) in self.relations.iter() { w.write_with_tag(74, |w| w.write_map(2 + sizeof_varint(*(k) as u64) + sizeof_len((v).get_size()), 8, |w| w.write_enum(*k as i32), 18, |w| w.write_message(v)))?; }
        for s in &self.author { w.write_with_tag(82, |w| w.write_string(&**s))?; }
        for s in &self.artist { w.write_with_tag(90, |w| w.write_string(&**s))?; }
        for s in &self.chapters { w.write_with_tag(122, |w| w.write_message(s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct ImageValue {
    pub num: u32,
    pub type_pb: enums::PageImageType,
    pub width: u32,
    pub height: u32,
}

impl<'a> MessageRead<'a> for ImageValue {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(0) => msg.num = r.read_uint32(bytes)?,
                Ok(8) => msg.type_pb = r.read_enum(bytes)?,
                Ok(16) => msg.width = r.read_uint32(bytes)?,
                Ok(24) => msg.height = r.read_uint32(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for ImageValue {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_varint(*(&self.num) as u64)
        + 1 + sizeof_varint(*(&self.type_pb) as u64)
        + 1 + sizeof_varint(*(&self.width) as u64)
        + 1 + sizeof_varint(*(&self.height) as u64)
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(0, |w| w.write_uint32(*&self.num))?;
        w.write_with_tag(8, |w| w.write_enum(*&self.type_pb as i32))?;
        w.write_with_tag(16, |w| w.write_uint32(*&self.width))?;
        w.write_with_tag(24, |w| w.write_uint32(*&self.height))?;
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct ChapterData<'a> {
    pub hash: Cow<'a, str>,
    pub prefix: Cow<'a, str>,
    pub start_from: u32,
    pub is_ok: bool,
    pub pages: Vec<ImageValue>,
    pub server: mod_ChapterData::OneOfserver<'a>,
}

impl<'a> MessageRead<'a> for ChapterData<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.hash = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(42) => msg.prefix = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(48) => msg.start_from = r.read_uint32(bytes)?,
                Ok(112) => msg.is_ok = r.read_bool(bytes)?,
                Ok(122) => msg.pages.push(r.read_message::<ImageValue>(bytes)?),
                Ok(16) => msg.server = mod_ChapterData::OneOfserver::s_mangadex(r.read_uint32(bytes)?),
                Ok(24) => msg.server = mod_ChapterData::OneOfserver::cdndex_or_else_data(r.read_bool(bytes)?),
                Ok(34) => msg.server = mod_ChapterData::OneOfserver::other(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for ChapterData<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_len((&self.hash).len())
        + 1 + sizeof_len((&self.prefix).len())
        + 1 + sizeof_varint(*(&self.start_from) as u64)
        + 1 + sizeof_varint(*(&self.is_ok) as u64)
        + self.pages.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + match self.server {
            mod_ChapterData::OneOfserver::s_mangadex(ref m) => 1 + sizeof_varint(*(m) as u64),
            mod_ChapterData::OneOfserver::cdndex_or_else_data(ref m) => 1 + sizeof_varint(*(m) as u64),
            mod_ChapterData::OneOfserver::other(ref m) => 1 + sizeof_len((m).len()),
            mod_ChapterData::OneOfserver::None => 0,
    }    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(10, |w| w.write_string(&**&self.hash))?;
        w.write_with_tag(42, |w| w.write_string(&**&self.prefix))?;
        w.write_with_tag(48, |w| w.write_uint32(*&self.start_from))?;
        w.write_with_tag(112, |w| w.write_bool(*&self.is_ok))?;
        for s in &self.pages { w.write_with_tag(122, |w| w.write_message(s))?; }
        match self.server {            mod_ChapterData::OneOfserver::s_mangadex(ref m) => { w.write_with_tag(16, |w| w.write_uint32(*m))? },
            mod_ChapterData::OneOfserver::cdndex_or_else_data(ref m) => { w.write_with_tag(24, |w| w.write_bool(*m))? },
            mod_ChapterData::OneOfserver::other(ref m) => { w.write_with_tag(34, |w| w.write_string(&**m))? },
            mod_ChapterData::OneOfserver::None => {},
    }        Ok(())
    }
}

pub mod mod_ChapterData {

use super::*;

#[derive(Debug, PartialEq, Clone)]
pub enum OneOfserver<'a> {
    s_mangadex(u32),
    cdndex_or_else_data(bool),
    other(Cow<'a, str>),
    None,
}

impl<'a> Default for OneOfserver<'a> {
    fn default() -> Self {
        OneOfserver::None
    }
}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Chapter<'a> {
    pub id: u32,
    pub timestamp: u32,
    pub lang: Option<enums::LangFlag>,
    pub user: HashMap<u32, Cow<'a, str>>,
    pub group: HashMap<u32, Cow<'a, str>>,
    pub title: Cow<'a, str>,
    pub chapter: Cow<'a, str>,
    pub volume: Option<u32>,
}

impl<'a> MessageRead<'a> for Chapter<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.id = r.read_uint32(bytes)?,
                Ok(21) => msg.timestamp = r.read_fixed32(bytes)?,
                Ok(24) => msg.lang = Some(r.read_enum(bytes)?),
                Ok(34) => {
                    let (key, value) = r.read_map(bytes, |r, bytes| r.read_uint32(bytes), |r, bytes| r.read_string(bytes).map(Cow::Borrowed))?;
                    msg.user.insert(key, value);
                }
                Ok(42) => {
                    let (key, value) = r.read_map(bytes, |r, bytes| r.read_uint32(bytes), |r, bytes| r.read_string(bytes).map(Cow::Borrowed))?;
                    msg.group.insert(key, value);
                }
                Ok(50) => msg.title = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(58) => msg.chapter = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(64) => msg.volume = Some(r.read_uint32(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Chapter<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_varint(*(&self.id) as u64)
        + 1 + 4
        + self.lang.as_ref().map_or(0, |m| 1 + sizeof_varint(*(m) as u64))
        + self.user.iter().map(|(k, v)| 1 + sizeof_len(2 + sizeof_varint(*(k) as u64) + sizeof_len((v).len()))).sum::<usize>()
        + self.group.iter().map(|(k, v)| 1 + sizeof_len(2 + sizeof_varint(*(k) as u64) + sizeof_len((v).len()))).sum::<usize>()
        + 1 + sizeof_len((&self.title).len())
        + 1 + sizeof_len((&self.chapter).len())
        + self.volume.as_ref().map_or(0, |m| 1 + sizeof_varint(*(m) as u64))
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(8, |w| w.write_uint32(*&self.id))?;
        w.write_with_tag(21, |w| w.write_fixed32(*&self.timestamp))?;
        if let Some(ref s) = self.lang { w.write_with_tag(24, |w| w.write_enum(*s as i32))?; }
        for (k, v) in self.user.iter() { w.write_with_tag(34, |w| w.write_map(2 + sizeof_varint(*(k) as u64) + sizeof_len((v).len()), 8, |w| w.write_uint32(*k), 18, |w| w.write_string(&**v)))?; }
        for (k, v) in self.group.iter() { w.write_with_tag(42, |w| w.write_map(2 + sizeof_varint(*(k) as u64) + sizeof_len((v).len()), 8, |w| w.write_uint32(*k), 18, |w| w.write_string(&**v)))?; }
        w.write_with_tag(50, |w| w.write_string(&**&self.title))?;
        w.write_with_tag(58, |w| w.write_string(&**&self.chapter))?;
        if let Some(ref s) = self.volume { w.write_with_tag(64, |w| w.write_uint32(*s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Comment<'a> {
    pub id: u32,
    pub timestamp: u32,
    pub content: Cow<'a, str>,
}

impl<'a> MessageRead<'a> for Comment<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.id = r.read_uint32(bytes)?,
                Ok(21) => msg.timestamp = r.read_fixed32(bytes)?,
                Ok(26) => msg.content = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Comment<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_varint(*(&self.id) as u64)
        + 1 + 4
        + 1 + sizeof_len((&self.content).len())
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(8, |w| w.write_uint32(*&self.id))?;
        w.write_with_tag(21, |w| w.write_fixed32(*&self.timestamp))?;
        w.write_with_tag(26, |w| w.write_string(&**&self.content))?;
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Thread<'a> {
    pub comments: Vec<Comment<'a>>,
}

impl<'a> MessageRead<'a> for Thread<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.comments.push(r.read_message::<Comment>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Thread<'a> {
    fn get_size(&self) -> usize {
        0
        + self.comments.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.comments { w.write_with_tag(10, |w| w.write_message(s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Response<'a> {
    pub resp: mod_Response::OneOfresp<'a>,
}

impl<'a> MessageRead<'a> for Response<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(2) => msg.resp = mod_Response::OneOfresp::err(r.read_message::<Error>(bytes)?),
                Ok(10) => msg.resp = mod_Response::OneOfresp::title(r.read_message::<Title>(bytes)?),
                Ok(18) => msg.resp = mod_Response::OneOfresp::chapter(r.read_message::<Chapter>(bytes)?),
                Ok(26) => msg.resp = mod_Response::OneOfresp::chapter_data(r.read_message::<ChapterData>(bytes)?),
                Ok(42) => msg.resp = mod_Response::OneOfresp::thread(r.read_message::<Thread>(bytes)?),
                Ok(50) => msg.resp = mod_Response::OneOfresp::unknown_json(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Response<'a> {
    fn get_size(&self) -> usize {
        0
        + match self.resp {
            mod_Response::OneOfresp::err(ref m) => 1 + sizeof_len((m).get_size()),
            mod_Response::OneOfresp::title(ref m) => 1 + sizeof_len((m).get_size()),
            mod_Response::OneOfresp::chapter(ref m) => 1 + sizeof_len((m).get_size()),
            mod_Response::OneOfresp::chapter_data(ref m) => 1 + sizeof_len((m).get_size()),
            mod_Response::OneOfresp::thread(ref m) => 1 + sizeof_len((m).get_size()),
            mod_Response::OneOfresp::unknown_json(ref m) => 1 + sizeof_len((m).len()),
            mod_Response::OneOfresp::None => 0,
    }    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        match self.resp {            mod_Response::OneOfresp::err(ref m) => { w.write_with_tag(2, |w| w.write_message(m))? },
            mod_Response::OneOfresp::title(ref m) => { w.write_with_tag(10, |w| w.write_message(m))? },
            mod_Response::OneOfresp::chapter(ref m) => { w.write_with_tag(18, |w| w.write_message(m))? },
            mod_Response::OneOfresp::chapter_data(ref m) => { w.write_with_tag(26, |w| w.write_message(m))? },
            mod_Response::OneOfresp::thread(ref m) => { w.write_with_tag(42, |w| w.write_message(m))? },
            mod_Response::OneOfresp::unknown_json(ref m) => { w.write_with_tag(50, |w| w.write_string(&**m))? },
            mod_Response::OneOfresp::None => {},
    }        Ok(())
    }
}

pub mod mod_Response {

use super::*;

#[derive(Debug, PartialEq, Clone)]
pub enum OneOfresp<'a> {
    err(Error<'a>),
    title(Title<'a>),
    chapter(Chapter<'a>),
    chapter_data(ChapterData<'a>),
    thread(Thread<'a>),
    unknown_json(Cow<'a, str>),
    None,
}

impl<'a> Default for OneOfresp<'a> {
    fn default() -> Self {
        OneOfresp::None
    }
}

}

