
use quick_protobuf::{BytesReader, Result, MessageRead, MessageWrite};
use super::*;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum PageImageType {
    UNKNOWN = 0,
    PNG = 1,
    JPG = 2,
    JPEG = 3,
    GIF = 4,
    SVG = 5,
    WEBP = 6,
}

impl Default for PageImageType {
    fn default() -> Self {
        PageImageType::UNKNOWN
    }
}

impl From<i32> for PageImageType {
    fn from(i: i32) -> Self {
        match i {
            0 => PageImageType::UNKNOWN,
            1 => PageImageType::PNG,
            2 => PageImageType::JPG,
            3 => PageImageType::JPEG,
            4 => PageImageType::GIF,
            5 => PageImageType::SVG,
            6 => PageImageType::WEBP,
            _ => Self::default(),
        }
    }
}

impl<'a> From<&'a str> for PageImageType {
    fn from(s: &'a str) -> Self {
        match s {
            "UNKNOWN" => PageImageType::UNKNOWN,
            "PNG" => PageImageType::PNG,
            "JPG" => PageImageType::JPG,
            "JPEG" => PageImageType::JPEG,
            "GIF" => PageImageType::GIF,
            "SVG" => PageImageType::SVG,
            "WEBP" => PageImageType::WEBP,
            _ => Self::default(),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Status {
    UNKNOWN = 0,
    ONGOING = 1,
    COMPLETED = 2,
    CANCELLED = 3,
    HIATUS = 4,
}

impl Default for Status {
    fn default() -> Self {
        Status::UNKNOWN
    }
}

impl From<i32> for Status {
    fn from(i: i32) -> Self {
        match i {
            0 => Status::UNKNOWN,
            1 => Status::ONGOING,
            2 => Status::COMPLETED,
            3 => Status::CANCELLED,
            4 => Status::HIATUS,
            _ => Self::default(),
        }
    }
}

impl<'a> From<&'a str> for Status {
    fn from(s: &'a str) -> Self {
        match s {
            "UNKNOWN" => Status::UNKNOWN,
            "ONGOING" => Status::ONGOING,
            "COMPLETED" => Status::COMPLETED,
            "CANCELLED" => Status::CANCELLED,
            "HIATUS" => Status::HIATUS,
            _ => Self::default(),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum ErrCode {
    OTHER = 0,
    SERVER_DOWN = 1,
    BAD_REQUEST = 2,
    NOT_FOUND = 3,
    GONE = 4,
    DDOS = 5,
}

impl Default for ErrCode {
    fn default() -> Self {
        ErrCode::OTHER
    }
}

impl From<i32> for ErrCode {
    fn from(i: i32) -> Self {
        match i {
            0 => ErrCode::OTHER,
            1 => ErrCode::SERVER_DOWN,
            2 => ErrCode::BAD_REQUEST,
            3 => ErrCode::NOT_FOUND,
            4 => ErrCode::GONE,
            5 => ErrCode::DDOS,
            _ => Self::default(),
        }
    }
}

impl<'a> From<&'a str> for ErrCode {
    fn from(s: &'a str) -> Self {
        match s {
            "OTHER" => ErrCode::OTHER,
            "SERVER_DOWN" => ErrCode::SERVER_DOWN,
            "BAD_REQUEST" => ErrCode::BAD_REQUEST,
            "NOT_FOUND" => ErrCode::NOT_FOUND,
            "GONE" => ErrCode::GONE,
            "DDOS" => ErrCode::DDOS,
            _ => Self::default(),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum LangFlag {
    NONE = 0,
    gb = 1,
    jp = 2,
    pl = 3,
    rs = 4,
    nl = 5,
    it = 6,
    ru = 7,
    de = 8,
    hu = 9,
    fr = 10,
    fi = 11,
    vn = 12,
    gr = 13,
    bg = 14,
    es = 15,
    br = 16,
    pt = 17,
    se = 18,
    sa = 19,
    dk = 20,
    cn = 21,
    bd = 22,
    ro = 23,
    cz = 24,
    mn = 25,
    tr = 26,
    id = 27,
    kr = 28,
    mx = 29,
    ir = 30,
    my = 31,
    th = 32,
    ct = 33,
    ph = 34,
    hk = 35,
    ua = 36,
    mm = 37,
    lt = 38,
}

impl Default for LangFlag {
    fn default() -> Self {
        LangFlag::NONE
    }
}

impl From<i32> for LangFlag {
    fn from(i: i32) -> Self {
        match i {
            0 => LangFlag::NONE,
            1 => LangFlag::gb,
            2 => LangFlag::jp,
            3 => LangFlag::pl,
            4 => LangFlag::rs,
            5 => LangFlag::nl,
            6 => LangFlag::it,
            7 => LangFlag::ru,
            8 => LangFlag::de,
            9 => LangFlag::hu,
            10 => LangFlag::fr,
            11 => LangFlag::fi,
            12 => LangFlag::vn,
            13 => LangFlag::gr,
            14 => LangFlag::bg,
            15 => LangFlag::es,
            16 => LangFlag::br,
            17 => LangFlag::pt,
            18 => LangFlag::se,
            19 => LangFlag::sa,
            20 => LangFlag::dk,
            21 => LangFlag::cn,
            22 => LangFlag::bd,
            23 => LangFlag::ro,
            24 => LangFlag::cz,
            25 => LangFlag::mn,
            26 => LangFlag::tr,
            27 => LangFlag::id,
            28 => LangFlag::kr,
            29 => LangFlag::mx,
            30 => LangFlag::ir,
            31 => LangFlag::my,
            32 => LangFlag::th,
            33 => LangFlag::ct,
            34 => LangFlag::ph,
            35 => LangFlag::hk,
            36 => LangFlag::ua,
            37 => LangFlag::mm,
            38 => LangFlag::lt,
            _ => Self::default(),
        }
    }
}

impl<'a> From<&'a str> for LangFlag {
    fn from(s: &'a str) -> Self {
        match s {
            "NONE" => LangFlag::NONE,
            "gb" => LangFlag::gb,
            "jp" => LangFlag::jp,
            "pl" => LangFlag::pl,
            "rs" => LangFlag::rs,
            "nl" => LangFlag::nl,
            "it" => LangFlag::it,
            "ru" => LangFlag::ru,
            "de" => LangFlag::de,
            "hu" => LangFlag::hu,
            "fr" => LangFlag::fr,
            "fi" => LangFlag::fi,
            "vn" => LangFlag::vn,
            "gr" => LangFlag::gr,
            "bg" => LangFlag::bg,
            "es" => LangFlag::es,
            "br" => LangFlag::br,
            "pt" => LangFlag::pt,
            "se" => LangFlag::se,
            "sa" => LangFlag::sa,
            "dk" => LangFlag::dk,
            "cn" => LangFlag::cn,
            "bd" => LangFlag::bd,
            "ro" => LangFlag::ro,
            "cz" => LangFlag::cz,
            "mn" => LangFlag::mn,
            "tr" => LangFlag::tr,
            "id" => LangFlag::id,
            "kr" => LangFlag::kr,
            "mx" => LangFlag::mx,
            "ir" => LangFlag::ir,
            "my" => LangFlag::my,
            "th" => LangFlag::th,
            "ct" => LangFlag::ct,
            "ph" => LangFlag::ph,
            "hk" => LangFlag::hk,
            "ua" => LangFlag::ua,
            "mm" => LangFlag::mm,
            "lt" => LangFlag::lt,
            _ => Self::default(),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum RelatedBy {
    SELF = 0,
    PREQUEL = 1,
    SEQUEL = 2,
    ADAPTED_FROM = 3,
    SPIN_OFF = 4,
    SIDE_STORY = 5,
    MAIN_STORY = 6,
    ALT_STORY = 7,
    DOUJINSHI = 8,
    BASED_ON = 9,
    COLOURED = 10,
    MONOCHROME = 11,
    SHARED_UNIVERSE = 12,
    SAME_FRANCHISE = 13,
    PRE_SERIALISATION = 14,
    SERIALISATION = 15,
}

impl Default for RelatedBy {
    fn default() -> Self {
        RelatedBy::SELF
    }
}

impl From<i32> for RelatedBy {
    fn from(i: i32) -> Self {
        match i {
            0 => RelatedBy::SELF,
            1 => RelatedBy::PREQUEL,
            2 => RelatedBy::SEQUEL,
            3 => RelatedBy::ADAPTED_FROM,
            4 => RelatedBy::SPIN_OFF,
            5 => RelatedBy::SIDE_STORY,
            6 => RelatedBy::MAIN_STORY,
            7 => RelatedBy::ALT_STORY,
            8 => RelatedBy::DOUJINSHI,
            9 => RelatedBy::BASED_ON,
            10 => RelatedBy::COLOURED,
            11 => RelatedBy::MONOCHROME,
            12 => RelatedBy::SHARED_UNIVERSE,
            13 => RelatedBy::SAME_FRANCHISE,
            14 => RelatedBy::PRE_SERIALISATION,
            15 => RelatedBy::SERIALISATION,
            _ => Self::default(),
        }
    }
}

impl<'a> From<&'a str> for RelatedBy {
    fn from(s: &'a str) -> Self {
        match s {
            "SELF" => RelatedBy::SELF,
            "PREQUEL" => RelatedBy::PREQUEL,
            "SEQUEL" => RelatedBy::SEQUEL,
            "ADAPTED_FROM" => RelatedBy::ADAPTED_FROM,
            "SPIN_OFF" => RelatedBy::SPIN_OFF,
            "SIDE_STORY" => RelatedBy::SIDE_STORY,
            "MAIN_STORY" => RelatedBy::MAIN_STORY,
            "ALT_STORY" => RelatedBy::ALT_STORY,
            "DOUJINSHI" => RelatedBy::DOUJINSHI,
            "BASED_ON" => RelatedBy::BASED_ON,
            "COLOURED" => RelatedBy::COLOURED,
            "MONOCHROME" => RelatedBy::MONOCHROME,
            "SHARED_UNIVERSE" => RelatedBy::SHARED_UNIVERSE,
            "SAME_FRANCHISE" => RelatedBy::SAME_FRANCHISE,
            "PRE_SERIALISATION" => RelatedBy::PRE_SERIALISATION,
            "SERIALISATION" => RelatedBy::SERIALISATION,
            _ => Self::default(),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Demographic {
    NONE = 0,
    SHOUNEN = 1,
    SHOUJO = 2,
    SEINEN = 3,
    JOUSEI = 4,
}

impl Default for Demographic {
    fn default() -> Self {
        Demographic::NONE
    }
}

impl From<i32> for Demographic {
    fn from(i: i32) -> Self {
        match i {
            0 => Demographic::NONE,
            1 => Demographic::SHOUNEN,
            2 => Demographic::SHOUJO,
            3 => Demographic::SEINEN,
            4 => Demographic::JOUSEI,
            _ => Self::default(),
        }
    }
}

impl<'a> From<&'a str> for Demographic {
    fn from(s: &'a str) -> Self {
        match s {
            "NONE" => Demographic::NONE,
            "SHOUNEN" => Demographic::SHOUNEN,
            "SHOUJO" => Demographic::SHOUJO,
            "SEINEN" => Demographic::SEINEN,
            "JOUSEI" => Demographic::JOUSEI,
            _ => Self::default(),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Genre {
    NONE = 0,
    FOUR_KOMA = 1,
    ACTION = 2,
    ADVENTURE = 3,
    AWARD_WINNING = 4,
    COMEDY = 5,
    COOKING = 6,
    DOUJINSHI = 7,
    DRAMA = 8,
    ECCHI = 9,
    FANTASY = 10,
    GYARU = 11,
    HAREM = 12,
    HISTORICAL = 13,
    HORROR = 14,
    MARTIAL_ARTS = 16,
    MECHA = 17,
    MEDICAL = 18,
    MUSIC = 19,
    MYSTERY = 20,
    ONESHOT = 21,
    PSYCHOLOGICAL = 22,
    ROMANCE = 23,
    SCHOOL_LIFE = 24,
    SCI_FI = 25,
    SHOUJO_AI = 28,
    SHOUNEN_AI = 30,
    SLICE_OF_LIFE = 31,
    SMUT = 32,
    SPORTS = 33,
    SUPERNATURAL = 34,
    TRAGEDY = 35,
    LONG_STRIP = 36,
    YAOI = 37,
    YURI = 38,
    VIDEO_GAMES = 40,
    ISEKAI = 41,
    ADAPTATION = 42,
    ANTHOLOGY = 43,
    WEB_COMIC = 44,
    FULL_COLOR = 45,
    USER_CREATED = 46,
    OFFICIAL_COLORED = 47,
    FAN_COLORED = 48,
    GORE = 49,
    SEXUAL_VIOLENCE = 50,
    CRIME = 51,
    MAGICAL_GIRLS = 52,
    PHILOSOPHICAL = 53,
    SUPERHERO = 54,
    THRILLER = 55,
    WUXIA = 56,
    ALIENS = 57,
    ANIMALS = 58,
    CROSSDRESSING = 59,
    DEMONS = 60,
    DELINQUENTS = 61,
    GENDERSWAP = 62,
    GHOSTS = 63,
    MONSTER_GIRLS = 64,
    LOLI = 65,
    MAGIC = 66,
    MILITARY = 67,
    MONSTERS = 68,
    NINJA = 69,
    OFFICE_WORKERS = 70,
    POLICE = 71,
    POST_APOCALYPTIC = 72,
    REINCARNATION = 73,
    REVERSE_HAREM = 74,
    SAMURAI = 75,
    SHOTA = 76,
    SURVIVAL = 77,
    TIME_TRAVEL = 78,
    VAMPIRES = 79,
    TRADITIONAL_GAMES = 80,
    VIRTUAL_REALITY = 81,
    ZOMBIES = 82,
    INCEST = 83,
}

impl Default for Genre {
    fn default() -> Self {
        Genre::NONE
    }
}

impl From<i32> for Genre {
    fn from(i: i32) -> Self {
        match i {
            0 => Genre::NONE,
            1 => Genre::FOUR_KOMA,
            2 => Genre::ACTION,
            3 => Genre::ADVENTURE,
            4 => Genre::AWARD_WINNING,
            5 => Genre::COMEDY,
            6 => Genre::COOKING,
            7 => Genre::DOUJINSHI,
            8 => Genre::DRAMA,
            9 => Genre::ECCHI,
            10 => Genre::FANTASY,
            11 => Genre::GYARU,
            12 => Genre::HAREM,
            13 => Genre::HISTORICAL,
            14 => Genre::HORROR,
            16 => Genre::MARTIAL_ARTS,
            17 => Genre::MECHA,
            18 => Genre::MEDICAL,
            19 => Genre::MUSIC,
            20 => Genre::MYSTERY,
            21 => Genre::ONESHOT,
            22 => Genre::PSYCHOLOGICAL,
            23 => Genre::ROMANCE,
            24 => Genre::SCHOOL_LIFE,
            25 => Genre::SCI_FI,
            28 => Genre::SHOUJO_AI,
            30 => Genre::SHOUNEN_AI,
            31 => Genre::SLICE_OF_LIFE,
            32 => Genre::SMUT,
            33 => Genre::SPORTS,
            34 => Genre::SUPERNATURAL,
            35 => Genre::TRAGEDY,
            36 => Genre::LONG_STRIP,
            37 => Genre::YAOI,
            38 => Genre::YURI,
            40 => Genre::VIDEO_GAMES,
            41 => Genre::ISEKAI,
            42 => Genre::ADAPTATION,
            43 => Genre::ANTHOLOGY,
            44 => Genre::WEB_COMIC,
            45 => Genre::FULL_COLOR,
            46 => Genre::USER_CREATED,
            47 => Genre::OFFICIAL_COLORED,
            48 => Genre::FAN_COLORED,
            49 => Genre::GORE,
            50 => Genre::SEXUAL_VIOLENCE,
            51 => Genre::CRIME,
            52 => Genre::MAGICAL_GIRLS,
            53 => Genre::PHILOSOPHICAL,
            54 => Genre::SUPERHERO,
            55 => Genre::THRILLER,
            56 => Genre::WUXIA,
            57 => Genre::ALIENS,
            58 => Genre::ANIMALS,
            59 => Genre::CROSSDRESSING,
            60 => Genre::DEMONS,
            61 => Genre::DELINQUENTS,
            62 => Genre::GENDERSWAP,
            63 => Genre::GHOSTS,
            64 => Genre::MONSTER_GIRLS,
            65 => Genre::LOLI,
            66 => Genre::MAGIC,
            67 => Genre::MILITARY,
            68 => Genre::MONSTERS,
            69 => Genre::NINJA,
            70 => Genre::OFFICE_WORKERS,
            71 => Genre::POLICE,
            72 => Genre::POST_APOCALYPTIC,
            73 => Genre::REINCARNATION,
            74 => Genre::REVERSE_HAREM,
            75 => Genre::SAMURAI,
            76 => Genre::SHOTA,
            77 => Genre::SURVIVAL,
            78 => Genre::TIME_TRAVEL,
            79 => Genre::VAMPIRES,
            80 => Genre::TRADITIONAL_GAMES,
            81 => Genre::VIRTUAL_REALITY,
            82 => Genre::ZOMBIES,
            83 => Genre::INCEST,
            _ => Self::default(),
        }
    }
}

impl<'a> From<&'a str> for Genre {
    fn from(s: &'a str) -> Self {
        match s {
            "NONE" => Genre::NONE,
            "FOUR_KOMA" => Genre::FOUR_KOMA,
            "ACTION" => Genre::ACTION,
            "ADVENTURE" => Genre::ADVENTURE,
            "AWARD_WINNING" => Genre::AWARD_WINNING,
            "COMEDY" => Genre::COMEDY,
            "COOKING" => Genre::COOKING,
            "DOUJINSHI" => Genre::DOUJINSHI,
            "DRAMA" => Genre::DRAMA,
            "ECCHI" => Genre::ECCHI,
            "FANTASY" => Genre::FANTASY,
            "GYARU" => Genre::GYARU,
            "HAREM" => Genre::HAREM,
            "HISTORICAL" => Genre::HISTORICAL,
            "HORROR" => Genre::HORROR,
            "MARTIAL_ARTS" => Genre::MARTIAL_ARTS,
            "MECHA" => Genre::MECHA,
            "MEDICAL" => Genre::MEDICAL,
            "MUSIC" => Genre::MUSIC,
            "MYSTERY" => Genre::MYSTERY,
            "ONESHOT" => Genre::ONESHOT,
            "PSYCHOLOGICAL" => Genre::PSYCHOLOGICAL,
            "ROMANCE" => Genre::ROMANCE,
            "SCHOOL_LIFE" => Genre::SCHOOL_LIFE,
            "SCI_FI" => Genre::SCI_FI,
            "SHOUJO_AI" => Genre::SHOUJO_AI,
            "SHOUNEN_AI" => Genre::SHOUNEN_AI,
            "SLICE_OF_LIFE" => Genre::SLICE_OF_LIFE,
            "SMUT" => Genre::SMUT,
            "SPORTS" => Genre::SPORTS,
            "SUPERNATURAL" => Genre::SUPERNATURAL,
            "TRAGEDY" => Genre::TRAGEDY,
            "LONG_STRIP" => Genre::LONG_STRIP,
            "YAOI" => Genre::YAOI,
            "YURI" => Genre::YURI,
            "VIDEO_GAMES" => Genre::VIDEO_GAMES,
            "ISEKAI" => Genre::ISEKAI,
            "ADAPTATION" => Genre::ADAPTATION,
            "ANTHOLOGY" => Genre::ANTHOLOGY,
            "WEB_COMIC" => Genre::WEB_COMIC,
            "FULL_COLOR" => Genre::FULL_COLOR,
            "USER_CREATED" => Genre::USER_CREATED,
            "OFFICIAL_COLORED" => Genre::OFFICIAL_COLORED,
            "FAN_COLORED" => Genre::FAN_COLORED,
            "GORE" => Genre::GORE,
            "SEXUAL_VIOLENCE" => Genre::SEXUAL_VIOLENCE,
            "CRIME" => Genre::CRIME,
            "MAGICAL_GIRLS" => Genre::MAGICAL_GIRLS,
            "PHILOSOPHICAL" => Genre::PHILOSOPHICAL,
            "SUPERHERO" => Genre::SUPERHERO,
            "THRILLER" => Genre::THRILLER,
            "WUXIA" => Genre::WUXIA,
            "ALIENS" => Genre::ALIENS,
            "ANIMALS" => Genre::ANIMALS,
            "CROSSDRESSING" => Genre::CROSSDRESSING,
            "DEMONS" => Genre::DEMONS,
            "DELINQUENTS" => Genre::DELINQUENTS,
            "GENDERSWAP" => Genre::GENDERSWAP,
            "GHOSTS" => Genre::GHOSTS,
            "MONSTER_GIRLS" => Genre::MONSTER_GIRLS,
            "LOLI" => Genre::LOLI,
            "MAGIC" => Genre::MAGIC,
            "MILITARY" => Genre::MILITARY,
            "MONSTERS" => Genre::MONSTERS,
            "NINJA" => Genre::NINJA,
            "OFFICE_WORKERS" => Genre::OFFICE_WORKERS,
            "POLICE" => Genre::POLICE,
            "POST_APOCALYPTIC" => Genre::POST_APOCALYPTIC,
            "REINCARNATION" => Genre::REINCARNATION,
            "REVERSE_HAREM" => Genre::REVERSE_HAREM,
            "SAMURAI" => Genre::SAMURAI,
            "SHOTA" => Genre::SHOTA,
            "SURVIVAL" => Genre::SURVIVAL,
            "TIME_TRAVEL" => Genre::TIME_TRAVEL,
            "VAMPIRES" => Genre::VAMPIRES,
            "TRADITIONAL_GAMES" => Genre::TRADITIONAL_GAMES,
            "VIRTUAL_REALITY" => Genre::VIRTUAL_REALITY,
            "ZOMBIES" => Genre::ZOMBIES,
            "INCEST" => Genre::INCEST,
            _ => Self::default(),
        }
    }
}

