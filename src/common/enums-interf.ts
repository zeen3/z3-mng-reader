export const enum Action {
	// Noop
	None = 0,

	// Scrolling
	ScrollDown,
	ScrollRight,
	ScrollUp,
	ScrollLeft,

	// Scrolling by pages. Default is 80% the innerWidth/Height axis.
	PageDown,
	PageRight,
	PageUp,
	PageLeft,

	// Paging
	PageNext,
	PagePrev,
	PageFirst,
	PageLast,

	// Chapter jump
	ChNext,
	ChPrev,
	ChFirst,
	ChLast,

	ChUnread,
	ChOldest,
	ChLatest,

	// Reading controls
	FullscreenEnable,
	FullscreenDisable,
	FullscreenToggle,

	ViewCycle,
	ViewWebtoon,
	ViewRTL,
	ViewLTR,
	ViewVert,

	SidebarToggle,
	NavbarToggle,
	ProgressToggle,

	// Loading
	Refresh,
	Cancel,

	// Saving
	Download,
	Delete,
	Enter,

	// Dialog boxes
	ToggleOptionsDialogue,

	// No further enum
	NoContinue
}

export const enum Mod {
	None = 0,
	Shift = 1 << 0,
	Meta = 1 << 1,
	ShiftMeta = Shift | Meta,

	Ctrl = 1 << 2,
	ShiftCtrl = Shift | Ctrl,
	MetaCtrl = Meta | Ctrl,

	ShiftMetaCtrl = Shift | Meta | Ctrl,
	Alt = 1 << 3,

	ShiftAlt = Shift | Alt,
	MetaAlt = Meta | Alt,
	ShiftMetaAlt = Shift | Meta | Alt,
	CtrlAlt = Ctrl | Alt,

	ShiftCtrlAlt = Shift | Ctrl | Alt,
	MetaCtrlAlt = Ctrl | Meta | Alt,
	ShiftMetaCtrlAlt = Shift | Meta | Ctrl | Alt,
}

export interface Actions extends Uint8Array {
	length: 16;
	byteLength: 16;
	[index: number]: Action;
}
