import { Actions, Action, Mod } from './enums-interf';

export type KeyCodeActions = {
    [s in KeyCode]?: Actions;
};
export type LooseKeyCodeActions = {
    [index: string]: Actions;
}
export type KeyCodeActionArray = {
    [s in KeyCode]?: readonly Action[];
};
export interface ActionsCode { code: KeyCode; actions: Actions; }

export type KeyCode =
    "AltLeft" | "AltRight" |
    "ArrowDown" | "ArrowLeft" | "ArrowRight" | "ArrowUp" |
    "Backquote" |
    "Backslash" |
    "Backspace" |
    "BracketLeft" | "BracketRight" |
    "CapsLock" |
    "Comma" |
    "ContextMenu" |
    "ControlLeft" | "ControlRight" |
    "Delete" |
    "Digit0" | "Digit1" | "Digit2" | "Digit3" | "Digit4" | "Digit5" | "Digit6" | "Digit7" | "Digit8" | "Digit9" |
    "End" |
    "Enter" |
    "Equal" |
    "Escape" |
    "F1" | "F2" | "F3" | "F4" | "F5" | "F6" | "F7" | "F8" | "F9" | "F10" | "F11" | "F12" | "F13" | "F14" | "F15" | "F16" | "F17" | "F18" |
    "Home" |
    "Insert" |
    "IntlBackslash" |
    "KeyA" | "KeyB" | "KeyC" | "KeyD" | "KeyE" | "KeyF" | "KeyG" | "KeyH" | "KeyI" | "KeyJ" | "KeyK" | "KeyL" | "KeyM" | "KeyN" | "KeyO" | "KeyP" | "KeyQ" | "KeyR" | "KeyS" | "KeyT" | "KeyU" | "KeyV" | "KeyW" | "KeyX" | "KeyY" | "KeyZ" |
    "MetaLeft" | "MetaRight" |
    "Minus" |
    "NumLock" |
    "Numpad0" | "Numpad1" | "Numpad2" | "Numpad3" | "Numpad4" | "Numpad5" | "Numpad6" | "Numpad7" | "Numpad8" | "Numpad9" |
    "NumpadAdd" |
    "NumpadDecimal" |
    "NumpadDivide" |
    "NumpadEnter" |
    "NumpadMultiply" |
    "NumpadSubtract" |
    "PageDown" | "PageUp" |
    "Pause" |
    "Period" |
    "Quote" |
    "ScrollLock" |
    "Semicolon" |
    "ShiftLeft" | "ShiftRight" |
    "Slash" |
    "Space" |
    "Tab"


declare var KEY_BINDINGS: undefined | KeyCodeActions;
declare var SCROLL_SPEED: undefined | {x: number; y: number;};

interface BroadcastChannelEnum {
    "key-code-actions": { code: KeyCode; mod: Mod; action: Action; };
    "scroll-speeds": [number, number];
    "unknown": any;
}
interface MessageEventEnum<T extends keyof BroadcastChannelEnum> extends MessageEvent {
    data: BroadcastChannelEnum[T];
}
interface BroadcastChannelKeyed<T extends keyof BroadcastChannelEnum> {
    onmessage: null | ((e: MessageEventEnum<T>) => void);
    close(): void;
}
declare var BroadcastChannelKeyed: {
    new<T extends keyof BroadcastChannelEnum>(name: T): BroadcastChannelKeyed<T>;
    prototype: BroadcastChannel;
}
