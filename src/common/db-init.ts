import { openDB, DBSchema, IDBPDatabase, OpenDBCallbacks } from '../../node_modules/idb/build/esm/index'
import { Action, Actions, Mod } from './enums-interf'
import { ActionsCode, KeyCodeActionArray, KeyCode } from './keycodes';
export interface ReaderZ3 extends DBSchema {
	'key-binds': {
		key: KeyCode;
		//value: ActionsCode;
		//indexes: { 'by-code': KeyCode; }
		value: Actions;
	};
	'scroll-speeds': {
		key: "xy";
		value: [number, number];
	}
};
function* recastActions(a: KeyCodeActionArray): IterableIterator<ActionsCode> {
		for (const s in a) {
			const code = s as KeyCode | "";
			const actl = code && a[code] as unknown as Action[] | undefined;
			if (!(Array.isArray(actl) && code)) continue
			let actions = new Uint8Array(16) as Actions, i = 16;
			while (i--)
				if (i in actl && actl[i] > Action.None && actl[i] < Action.NoContinue)
					actions[i] = actl[i];
				else actions[i] = 0;
			yield {code, actions}
		}
}
let rz3o: OpenDBCallbacks<ReaderZ3>;
export function readerZ3(): Promise<IDBPDatabase<ReaderZ3>> {
	return openDB('readerZ3', 1, rz3o || (rz3o = {
		upgrade(db, old, _, t) {
			if (old !== null) {
				console.log('wut');
			}
			let keybinds = db.createObjectStore("key-binds");
			//keybinds.createIndex("by-code", "code", {unique: true});
			for (const key of recastActions({
				'KeyS': [Action.ScrollDown],
				'KeyD': [Action.ScrollRight],
				'KeyW': [Action.ScrollUp],
				'KeyA': [Action.ScrollLeft],

				'KeyP': [Action.PagePrev],
				'KeyN': [Action.PageNext],
				'End': [Action.PageLast],
				'Home': [Action.PageFirst],

				'KeyF': [Action.FullscreenToggle],
				'F11': [Action.FullscreenToggle],
				'Escape': [Action.FullscreenDisable],

				'Slash': [0, Action.ToggleOptionsDialogue],
			})) keybinds.put(key.actions, key.code)
			let scrollspeeds = db.createObjectStore("scroll-speeds")
			scrollspeeds.put([0.84, 0.94], "xy")
			return t.done
		}
	}))
}
export async function keyBindsSet(code: KeyCode, mod: Mod, action: Action) {
	const rz = await readerZ3();
	//const v = await rz.get("key-binds", code) || {code, actions: new Uint8Array(16) as Actions};
	const actions = await rz.get("key-binds", code) || new Uint8Array(16) as Actions
	// return early if nothing would happen
	if (actions[mod] === action) return;
	actions[mod] = action;
	if (!action && actions.every(v => v === 0))
		await rz.delete("key-binds", code);
	else
		await rz.put("key-binds", actions, code);
}

export async function scrollSpeedsSet(x: number, y: number) {
	await (await readerZ3()).put("scroll-speeds", [+x, +y], "xy");
}
